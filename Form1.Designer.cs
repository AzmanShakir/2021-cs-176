﻿namespace FYP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Project = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtPD = new System.Windows.Forms.TextBox();
            this.txtPT = new System.Windows.Forms.TextBox();
            this.lblPD = new System.Windows.Forms.Label();
            this.lblPT = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPU = new System.Windows.Forms.Button();
            this.btnPC = new System.Windows.Forms.Button();
            this.btnS = new System.Windows.Forms.Button();
            this.lblPTS = new System.Windows.Forms.Label();
            this.txtPTS = new System.Windows.Forms.TextBox();
            this.dataGridViewP = new System.Windows.Forms.DataGridView();
            this.EditProjects = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tAdvsior = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dateTimePickerA = new System.Windows.Forms.DateTimePicker();
            this.lblAde = new System.Windows.Forms.Label();
            this.COAD = new System.Windows.Forms.ComboBox();
            this.coAG = new System.Windows.Forms.ComboBox();
            this.txtAS = new System.Windows.Forms.TextBox();
            this.lblAS = new System.Windows.Forms.Label();
            this.lblAG = new System.Windows.Forms.Label();
            this.txtAE = new System.Windows.Forms.TextBox();
            this.lblAE = new System.Windows.Forms.Label();
            this.lblAD = new System.Windows.Forms.Label();
            this.txtAC = new System.Windows.Forms.TextBox();
            this.txtAL = new System.Windows.Forms.TextBox();
            this.txtAF = new System.Windows.Forms.TextBox();
            this.lblAL = new System.Windows.Forms.Label();
            this.lBLAC = new System.Windows.Forms.Label();
            this.lblAF = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAU = new System.Windows.Forms.Button();
            this.btnAC = new System.Windows.Forms.Button();
            this.btnAS = new System.Windows.Forms.Button();
            this.lblAsearchN = new System.Windows.Forms.Label();
            this.txtANS = new System.Windows.Forms.TextBox();
            this.dataGridViewAdvsior = new System.Windows.Forms.DataGridView();
            this.EDITAdvsior = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TPStudent = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.COMSG = new System.Windows.Forms.ComboBox();
            this.DATESD = new System.Windows.Forms.DateTimePicker();
            this.txtSR = new System.Windows.Forms.TextBox();
            this.lblSR = new System.Windows.Forms.Label();
            this.lblSG = new System.Windows.Forms.Label();
            this.txtSE = new System.Windows.Forms.TextBox();
            this.lblSE = new System.Windows.Forms.Label();
            this.lblSD = new System.Windows.Forms.Label();
            this.txTSC = new System.Windows.Forms.TextBox();
            this.txtSL = new System.Windows.Forms.TextBox();
            this.txtSF = new System.Windows.Forms.TextBox();
            this.lblSLN = new System.Windows.Forms.Label();
            this.lblSC = new System.Windows.Forms.Label();
            this.lblSNam = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSU = new System.Windows.Forms.Button();
            this.btnSC = new System.Windows.Forms.Button();
            this.lblSSR = new System.Windows.Forms.Label();
            this.txtSRS = new System.Windows.Forms.TextBox();
            this.btnSs = new System.Windows.Forms.Button();
            this.StudentGV = new System.Windows.Forms.DataGridView();
            this.EDIT = new System.Windows.Forms.DataGridViewButtonColumn();
            this.LandingPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.homepagelabel = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.tablelayout = new System.Windows.Forms.TabControl();
            this.tbProjectAdvsior = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.comboIA = new System.Windows.Forms.ComboBox();
            this.IndustrialAdvsior = new System.Windows.Forms.Label();
            this.comboCoAd = new System.Windows.Forms.ComboBox();
            this.CoAdvsior = new System.Windows.Forms.Label();
            this.dateTimePickerPA = new System.Windows.Forms.DateTimePicker();
            this.comPAAI = new System.Windows.Forms.ComboBox();
            this.comPAPI = new System.Windows.Forms.ComboBox();
            this.lblPAD = new System.Windows.Forms.Label();
            this.lblPAPI = new System.Windows.Forms.Label();
            this.MainAdvsior = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPAU = new System.Windows.Forms.Button();
            this.btnPAC = new System.Windows.Forms.Button();
            this.lblPAPIS = new System.Windows.Forms.Label();
            this.txtPAPI = new System.Windows.Forms.TextBox();
            this.btnPAS = new System.Windows.Forms.Button();
            this.dataGridViewPA = new System.Windows.Forms.DataGridView();
            this.tbCreateGroups = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dateTimePickerGA = new System.Windows.Forms.DateTimePicker();
            this.comGPGI = new System.Windows.Forms.ComboBox();
            this.comGPPI = new System.Windows.Forms.ComboBox();
            this.lblGPAD = new System.Windows.Forms.Label();
            this.lblGPPI = new System.Windows.Forms.Label();
            this.lblAGPGI = new System.Windows.Forms.Label();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btnGPU = new System.Windows.Forms.Button();
            this.btnGPC = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSerchStudentGroups = new System.Windows.Forms.TextBox();
            this.btnCGS = new System.Windows.Forms.Button();
            this.dataGridViewGP = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dateTimePickerGC = new System.Windows.Forms.DateTimePicker();
            this.lblGC = new System.Windows.Forms.Label();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btnGCU = new System.Windows.Forms.Button();
            this.btnGC = new System.Windows.Forms.Button();
            this.dataGridViewGC = new System.Windows.Forms.DataGridView();
            this.tbEvalutions = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtTW = new System.Windows.Forms.TextBox();
            this.txtTM = new System.Windows.Forms.TextBox();
            this.txtEN = new System.Windows.Forms.TextBox();
            this.lblETW = new System.Windows.Forms.Label();
            this.lblET = new System.Windows.Forms.Label();
            this.lblEN = new System.Windows.Forms.Label();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.btnEU = new System.Windows.Forms.Button();
            this.btnEA = new System.Windows.Forms.Button();
            this.lblENS = new System.Windows.Forms.Label();
            this.txtENS = new System.Windows.Forms.TextBox();
            this.btnES = new System.Windows.Forms.Button();
            this.dataGridViewE = new System.Windows.Forms.DataGridView();
            this.EditEvaultions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tbGroupEvalutions = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dateTimePickerED = new System.Windows.Forms.DateTimePicker();
            this.comGEGI = new System.Windows.Forms.ComboBox();
            this.comGEEI = new System.Windows.Forms.ComboBox();
            this.lblGEED = new System.Windows.Forms.Label();
            this.lblGEOM = new System.Windows.Forms.Label();
            this.lblGEEI = new System.Windows.Forms.Label();
            this.lblGEGI = new System.Windows.Forms.Label();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.btnGEU = new System.Windows.Forms.Button();
            this.btnGEC = new System.Windows.Forms.Button();
            this.lblGEGIS = new System.Windows.Forms.Label();
            this.txtGEGI = new System.Windows.Forms.TextBox();
            this.btnGES = new System.Windows.Forms.Button();
            this.dataGridViewGE = new System.Windows.Forms.DataGridView();
            this.EditGroupEvalutions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.errorProviderSFN = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderSE = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderSR = new System.Windows.Forms.ErrorProvider(this.components);
            this.errorProviderdate = new System.Windows.Forms.ErrorProvider(this.components);
            this.DeactivateStudent = new System.Windows.Forms.DataGridViewButtonColumn();
            this.txtGEOM = new System.Windows.Forms.NumericUpDown();
            this.Project.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewP)).BeginInit();
            this.tAdvsior.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdvsior)).BeginInit();
            this.TPStudent.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentGV)).BeginInit();
            this.LandingPage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tablelayout.SuspendLayout();
            this.tbProjectAdvsior.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPA)).BeginInit();
            this.tbCreateGroups.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGP)).BeginInit();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGC)).BeginInit();
            this.tbEvalutions.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewE)).BeginInit();
            this.tbGroupEvalutions.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSFN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGEOM)).BeginInit();
            this.SuspendLayout();
            // 
            // Project
            // 
            this.Project.Controls.Add(this.tableLayoutPanel8);
            this.Project.Location = new System.Drawing.Point(4, 22);
            this.Project.Name = "Project";
            this.Project.Padding = new System.Windows.Forms.Padding(3);
            this.Project.Size = new System.Drawing.Size(792, 424);
            this.Project.TabIndex = 5;
            this.Project.Text = "Projects";
            this.Project.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.dataGridViewP, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(786, 418);
            this.tableLayoutPanel8.TabIndex = 11111;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel9.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(782, 124);
            this.tableLayoutPanel9.TabIndex = 11111;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtPD);
            this.panel3.Controls.Add(this.txtPT);
            this.panel3.Controls.Add(this.lblPD);
            this.panel3.Controls.Add(this.lblPT);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(2, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(560, 120);
            this.panel3.TabIndex = 11111;
            // 
            // txtPD
            // 
            this.txtPD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtPD.Location = new System.Drawing.Point(101, 42);
            this.txtPD.Margin = new System.Windows.Forms.Padding(2);
            this.txtPD.Multiline = true;
            this.txtPD.Name = "txtPD";
            this.txtPD.Size = new System.Drawing.Size(421, 64);
            this.txtPD.TabIndex = 10;
            // 
            // txtPT
            // 
            this.txtPT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtPT.Location = new System.Drawing.Point(101, 13);
            this.txtPT.Margin = new System.Windows.Forms.Padding(2);
            this.txtPT.Name = "txtPT";
            this.txtPT.Size = new System.Drawing.Size(421, 23);
            this.txtPT.TabIndex = 9;
            // 
            // lblPD
            // 
            this.lblPD.AutoSize = true;
            this.lblPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPD.Location = new System.Drawing.Point(6, 42);
            this.lblPD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPD.Name = "lblPD";
            this.lblPD.Size = new System.Drawing.Size(79, 17);
            this.lblPD.TabIndex = 8;
            this.lblPD.Text = "Description";
            // 
            // lblPT
            // 
            this.lblPT.AutoSize = true;
            this.lblPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPT.Location = new System.Drawing.Point(6, 13);
            this.lblPT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPT.Name = "lblPT";
            this.lblPT.Size = new System.Drawing.Size(79, 17);
            this.lblPT.TabIndex = 6;
            this.lblPT.Text = "Project Tile";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.btnPU, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnPC, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.btnS, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.lblPTS, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtPTS, 1, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(214, 120);
            this.tableLayoutPanel10.TabIndex = 1002;
            // 
            // btnPU
            // 
            this.btnPU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPU.Location = new System.Drawing.Point(123, 8);
            this.btnPU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnPU.Name = "btnPU";
            this.btnPU.Size = new System.Drawing.Size(74, 32);
            this.btnPU.TabIndex = 12;
            this.btnPU.Text = "Load";
            this.btnPU.UseVisualStyleBackColor = true;
            this.btnPU.Click += new System.EventHandler(this.btnPU_Click);
            // 
            // btnPC
            // 
            this.btnPC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPC.Location = new System.Drawing.Point(15, 8);
            this.btnPC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnPC.Name = "btnPC";
            this.btnPC.Size = new System.Drawing.Size(76, 32);
            this.btnPC.TabIndex = 10;
            this.btnPC.Text = "Create";
            this.btnPC.UseVisualStyleBackColor = true;
            this.btnPC.Click += new System.EventHandler(this.btnPC_Click);
            // 
            // btnS
            // 
            this.btnS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnS.Location = new System.Drawing.Point(122, 92);
            this.btnS.Margin = new System.Windows.Forms.Padding(0);
            this.btnS.Name = "btnS";
            this.btnS.Size = new System.Drawing.Size(76, 24);
            this.btnS.TabIndex = 13;
            this.btnS.Text = "Search";
            this.btnS.UseVisualStyleBackColor = true;
            this.btnS.Click += new System.EventHandler(this.btnS_Click);
            // 
            // lblPTS
            // 
            this.lblPTS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPTS.AutoSize = true;
            this.lblPTS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPTS.Location = new System.Drawing.Point(12, 57);
            this.lblPTS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPTS.Name = "lblPTS";
            this.lblPTS.Size = new System.Drawing.Size(83, 17);
            this.lblPTS.TabIndex = 18;
            this.lblPTS.Text = "Project Title";
            // 
            // txtPTS
            // 
            this.txtPTS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtPTS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtPTS.Location = new System.Drawing.Point(109, 54);
            this.txtPTS.Margin = new System.Windows.Forms.Padding(2);
            this.txtPTS.Name = "txtPTS";
            this.txtPTS.Size = new System.Drawing.Size(103, 23);
            this.txtPTS.TabIndex = 19;
            // 
            // dataGridViewP
            // 
            this.dataGridViewP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditProjects});
            this.dataGridViewP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewP.Location = new System.Drawing.Point(8, 136);
            this.dataGridViewP.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridViewP.Name = "dataGridViewP";
            this.dataGridViewP.RowHeadersWidth = 51;
            this.dataGridViewP.RowTemplate.Height = 24;
            this.dataGridViewP.Size = new System.Drawing.Size(770, 274);
            this.dataGridViewP.TabIndex = 11111;
            this.dataGridViewP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewP_CellContentClick);
            // 
            // EditProjects
            // 
            this.EditProjects.HeaderText = "EditProjects";
            this.EditProjects.Name = "EditProjects";
            this.EditProjects.Text = "EditProjects";
            this.EditProjects.UseColumnTextForButtonValue = true;
            // 
            // tAdvsior
            // 
            this.tAdvsior.Controls.Add(this.tableLayoutPanel5);
            this.tAdvsior.Location = new System.Drawing.Point(4, 22);
            this.tAdvsior.Name = "tAdvsior";
            this.tAdvsior.Padding = new System.Windows.Forms.Padding(3);
            this.tAdvsior.Size = new System.Drawing.Size(792, 424);
            this.tAdvsior.TabIndex = 3;
            this.tAdvsior.Text = "Advsior";
            this.tAdvsior.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.dataGridViewAdvsior, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 258F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(786, 418);
            this.tableLayoutPanel5.TabIndex = 11111;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel6.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(782, 254);
            this.tableLayoutPanel6.TabIndex = 11111;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dateTimePickerA);
            this.panel2.Controls.Add(this.lblAde);
            this.panel2.Controls.Add(this.COAD);
            this.panel2.Controls.Add(this.coAG);
            this.panel2.Controls.Add(this.txtAS);
            this.panel2.Controls.Add(this.lblAS);
            this.panel2.Controls.Add(this.lblAG);
            this.panel2.Controls.Add(this.txtAE);
            this.panel2.Controls.Add(this.lblAE);
            this.panel2.Controls.Add(this.lblAD);
            this.panel2.Controls.Add(this.txtAC);
            this.panel2.Controls.Add(this.txtAL);
            this.panel2.Controls.Add(this.txtAF);
            this.panel2.Controls.Add(this.lblAL);
            this.panel2.Controls.Add(this.lBLAC);
            this.panel2.Controls.Add(this.lblAF);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(560, 250);
            this.panel2.TabIndex = 1111;
            // 
            // dateTimePickerA
            // 
            this.dateTimePickerA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerA.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerA.Location = new System.Drawing.Point(99, 136);
            this.dateTimePickerA.Name = "dateTimePickerA";
            this.dateTimePickerA.Size = new System.Drawing.Size(410, 20);
            this.dateTimePickerA.TabIndex = 23;
            this.dateTimePickerA.Value = new System.DateTime(2023, 3, 8, 0, 0, 0, 0);
            // 
            // lblAde
            // 
            this.lblAde.AutoSize = true;
            this.lblAde.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAde.Location = new System.Drawing.Point(6, 220);
            this.lblAde.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAde.Name = "lblAde";
            this.lblAde.Size = new System.Drawing.Size(83, 17);
            this.lblAde.TabIndex = 22;
            this.lblAde.Text = "Designation";
            // 
            // COAD
            // 
            this.COAD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.COAD.FormattingEnabled = true;
            this.COAD.Items.AddRange(new object[] {
            "Professor",
            "Associate Professor",
            "Assisstant Professor",
            "Lecturer",
            "Industry Professional"});
            this.COAD.Location = new System.Drawing.Point(99, 220);
            this.COAD.Name = "COAD";
            this.COAD.Size = new System.Drawing.Size(410, 21);
            this.COAD.TabIndex = 21;
            this.COAD.SelectedIndexChanged += new System.EventHandler(this.COAD_SelectedIndexChanged);
            // 
            // coAG
            // 
            this.coAG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.coAG.FormattingEnabled = true;
            this.coAG.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.coAG.Location = new System.Drawing.Point(99, 162);
            this.coAG.Name = "coAG";
            this.coAG.Size = new System.Drawing.Size(410, 21);
            this.coAG.TabIndex = 20;
            // 
            // txtAS
            // 
            this.txtAS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtAS.Location = new System.Drawing.Point(99, 188);
            this.txtAS.Margin = new System.Windows.Forms.Padding(2);
            this.txtAS.Name = "txtAS";
            this.txtAS.Size = new System.Drawing.Size(410, 23);
            this.txtAS.TabIndex = 18;
            // 
            // lblAS
            // 
            this.lblAS.AutoSize = true;
            this.lblAS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAS.Location = new System.Drawing.Point(6, 191);
            this.lblAS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAS.Name = "lblAS";
            this.lblAS.Size = new System.Drawing.Size(48, 17);
            this.lblAS.TabIndex = 17;
            this.lblAS.Text = "Salary";
            // 
            // lblAG
            // 
            this.lblAG.AutoSize = true;
            this.lblAG.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAG.Location = new System.Drawing.Point(6, 162);
            this.lblAG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAG.Name = "lblAG";
            this.lblAG.Size = new System.Drawing.Size(56, 17);
            this.lblAG.TabIndex = 16;
            this.lblAG.Text = "Gender";
            // 
            // txtAE
            // 
            this.txtAE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAE.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtAE.Location = new System.Drawing.Point(99, 108);
            this.txtAE.Margin = new System.Windows.Forms.Padding(2);
            this.txtAE.Name = "txtAE";
            this.txtAE.Size = new System.Drawing.Size(410, 23);
            this.txtAE.TabIndex = 14;
            // 
            // lblAE
            // 
            this.lblAE.AutoSize = true;
            this.lblAE.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAE.Location = new System.Drawing.Point(6, 108);
            this.lblAE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAE.Name = "lblAE";
            this.lblAE.Size = new System.Drawing.Size(42, 17);
            this.lblAE.TabIndex = 13;
            this.lblAE.Text = "Email";
            // 
            // lblAD
            // 
            this.lblAD.AutoSize = true;
            this.lblAD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAD.Location = new System.Drawing.Point(6, 136);
            this.lblAD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAD.Name = "lblAD";
            this.lblAD.Size = new System.Drawing.Size(48, 17);
            this.lblAD.TabIndex = 12;
            this.lblAD.Text = "D-O-B";
            // 
            // txtAC
            // 
            this.txtAC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtAC.Location = new System.Drawing.Point(99, 72);
            this.txtAC.Margin = new System.Windows.Forms.Padding(2);
            this.txtAC.Name = "txtAC";
            this.txtAC.Size = new System.Drawing.Size(410, 23);
            this.txtAC.TabIndex = 11;
            // 
            // txtAL
            // 
            this.txtAL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtAL.Location = new System.Drawing.Point(99, 42);
            this.txtAL.Margin = new System.Windows.Forms.Padding(2);
            this.txtAL.Name = "txtAL";
            this.txtAL.Size = new System.Drawing.Size(410, 23);
            this.txtAL.TabIndex = 10;
            // 
            // txtAF
            // 
            this.txtAF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtAF.Location = new System.Drawing.Point(99, 13);
            this.txtAF.Margin = new System.Windows.Forms.Padding(2);
            this.txtAF.Name = "txtAF";
            this.txtAF.Size = new System.Drawing.Size(410, 23);
            this.txtAF.TabIndex = 9;
            this.txtAF.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // lblAL
            // 
            this.lblAL.AutoSize = true;
            this.lblAL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAL.Location = new System.Drawing.Point(6, 42);
            this.lblAL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAL.Name = "lblAL";
            this.lblAL.Size = new System.Drawing.Size(76, 17);
            this.lblAL.TabIndex = 8;
            this.lblAL.Text = "Last Name";
            this.lblAL.Click += new System.EventHandler(this.label7_Click);
            // 
            // lBLAC
            // 
            this.lBLAC.AutoSize = true;
            this.lBLAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lBLAC.Location = new System.Drawing.Point(6, 75);
            this.lBLAC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lBLAC.Name = "lBLAC";
            this.lBLAC.Size = new System.Drawing.Size(56, 17);
            this.lBLAC.TabIndex = 7;
            this.lBLAC.Text = "Contact";
            // 
            // lblAF
            // 
            this.lblAF.AutoSize = true;
            this.lblAF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAF.Location = new System.Drawing.Point(6, 13);
            this.lblAF.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAF.Name = "lblAF";
            this.lblAF.Size = new System.Drawing.Size(76, 17);
            this.lblAF.TabIndex = 6;
            this.lblAF.Text = "First Name";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.btnAU, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnAC, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnAS, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.lblAsearchN, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.txtANS, 1, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(214, 250);
            this.tableLayoutPanel7.TabIndex = 11111;
            // 
            // btnAU
            // 
            this.btnAU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAU.Location = new System.Drawing.Point(123, 24);
            this.btnAU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnAU.Name = "btnAU";
            this.btnAU.Size = new System.Drawing.Size(74, 37);
            this.btnAU.TabIndex = 12;
            this.btnAU.Text = "LOAD";
            this.btnAU.UseVisualStyleBackColor = true;
            this.btnAU.Click += new System.EventHandler(this.btnAU_Click);
            // 
            // btnAC
            // 
            this.btnAC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAC.Location = new System.Drawing.Point(15, 23);
            this.btnAC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnAC.Name = "btnAC";
            this.btnAC.Size = new System.Drawing.Size(76, 39);
            this.btnAC.TabIndex = 10;
            this.btnAC.TabStop = false;
            this.btnAC.Text = "Create";
            this.btnAC.UseVisualStyleBackColor = true;
            this.btnAC.Click += new System.EventHandler(this.btnAC_Click);
            // 
            // btnAS
            // 
            this.btnAS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnAS.Location = new System.Drawing.Point(122, 185);
            this.btnAS.Margin = new System.Windows.Forms.Padding(0);
            this.btnAS.Name = "btnAS";
            this.btnAS.Size = new System.Drawing.Size(76, 43);
            this.btnAS.TabIndex = 13;
            this.btnAS.Text = "Search";
            this.btnAS.UseVisualStyleBackColor = true;
            this.btnAS.Click += new System.EventHandler(this.btnAS_Click);
            // 
            // lblAsearchN
            // 
            this.lblAsearchN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAsearchN.AutoSize = true;
            this.lblAsearchN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAsearchN.Location = new System.Drawing.Point(15, 114);
            this.lblAsearchN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAsearchN.Name = "lblAsearchN";
            this.lblAsearchN.Size = new System.Drawing.Size(76, 17);
            this.lblAsearchN.TabIndex = 18;
            this.lblAsearchN.Text = "First Name";
            // 
            // txtANS
            // 
            this.txtANS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtANS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtANS.Location = new System.Drawing.Point(109, 111);
            this.txtANS.Margin = new System.Windows.Forms.Padding(2);
            this.txtANS.Name = "txtANS";
            this.txtANS.Size = new System.Drawing.Size(103, 23);
            this.txtANS.TabIndex = 19;
            this.txtANS.TextChanged += new System.EventHandler(this.txtANS_TextChanged);
            // 
            // dataGridViewAdvsior
            // 
            this.dataGridViewAdvsior.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAdvsior.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdvsior.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EDITAdvsior});
            this.dataGridViewAdvsior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewAdvsior.Location = new System.Drawing.Point(3, 261);
            this.dataGridViewAdvsior.Name = "dataGridViewAdvsior";
            this.dataGridViewAdvsior.Size = new System.Drawing.Size(780, 154);
            this.dataGridViewAdvsior.TabIndex = 11111;
            this.dataGridViewAdvsior.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAdvsior_CellContentClick);
            // 
            // EDITAdvsior
            // 
            this.EDITAdvsior.HeaderText = "EDITAdvsior";
            this.EDITAdvsior.Name = "EDITAdvsior";
            this.EDITAdvsior.Text = "EDITAdvsior";
            this.EDITAdvsior.ToolTipText = "EDITAdvsior";
            this.EDITAdvsior.UseColumnTextForButtonValue = true;
            // 
            // TPStudent
            // 
            this.TPStudent.Controls.Add(this.tableLayoutPanel2);
            this.TPStudent.Font = new System.Drawing.Font("MS Reference Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TPStudent.Location = new System.Drawing.Point(4, 22);
            this.TPStudent.Margin = new System.Windows.Forms.Padding(2);
            this.TPStudent.Name = "TPStudent";
            this.TPStudent.Padding = new System.Windows.Forms.Padding(2);
            this.TPStudent.Size = new System.Drawing.Size(792, 424);
            this.TPStudent.TabIndex = 2;
            this.TPStudent.Text = "Student";
            this.TPStudent.UseVisualStyleBackColor = true;
            this.TPStudent.Enter += new System.EventHandler(this.TPStudent_Enter);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.StudentGV, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 258F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(788, 420);
            this.tableLayoutPanel2.TabIndex = 11111;
            this.tableLayoutPanel2.TabStop = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(784, 254);
            this.tableLayoutPanel3.TabIndex = 11111;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.COMSG);
            this.panel1.Controls.Add(this.DATESD);
            this.panel1.Controls.Add(this.txtSR);
            this.panel1.Controls.Add(this.lblSR);
            this.panel1.Controls.Add(this.lblSG);
            this.panel1.Controls.Add(this.txtSE);
            this.panel1.Controls.Add(this.lblSE);
            this.panel1.Controls.Add(this.lblSD);
            this.panel1.Controls.Add(this.txTSC);
            this.panel1.Controls.Add(this.txtSL);
            this.panel1.Controls.Add(this.txtSF);
            this.panel1.Controls.Add(this.lblSLN);
            this.panel1.Controls.Add(this.lblSC);
            this.panel1.Controls.Add(this.lblSNam);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(562, 250);
            this.panel1.TabIndex = 1111;
            // 
            // COMSG
            // 
            this.COMSG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.COMSG.FormattingEnabled = true;
            this.COMSG.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.COMSG.Location = new System.Drawing.Point(101, 178);
            this.COMSG.Name = "COMSG";
            this.COMSG.Size = new System.Drawing.Size(420, 23);
            this.COMSG.TabIndex = 20;
            // 
            // DATESD
            // 
            this.DATESD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DATESD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DATESD.Location = new System.Drawing.Point(101, 147);
            this.DATESD.Name = "DATESD";
            this.DATESD.Size = new System.Drawing.Size(420, 20);
            this.DATESD.TabIndex = 19;
            this.DATESD.Value = new System.DateTime(2023, 3, 8, 0, 0, 0, 0);
            // 
            // txtSR
            // 
            this.txtSR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSR.Location = new System.Drawing.Point(101, 211);
            this.txtSR.Margin = new System.Windows.Forms.Padding(2);
            this.txtSR.Name = "txtSR";
            this.txtSR.Size = new System.Drawing.Size(420, 23);
            this.txtSR.TabIndex = 18;
            // 
            // lblSR
            // 
            this.lblSR.AutoSize = true;
            this.lblSR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSR.Location = new System.Drawing.Point(6, 211);
            this.lblSR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSR.Name = "lblSR";
            this.lblSR.Size = new System.Drawing.Size(92, 17);
            this.lblSR.TabIndex = 17;
            this.lblSR.Text = "Reg. Number";
            this.lblSR.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblSG
            // 
            this.lblSG.AutoSize = true;
            this.lblSG.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSG.Location = new System.Drawing.Point(6, 178);
            this.lblSG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSG.Name = "lblSG";
            this.lblSG.Size = new System.Drawing.Size(56, 17);
            this.lblSG.TabIndex = 16;
            this.lblSG.Text = "Gender";
            // 
            // txtSE
            // 
            this.txtSE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSE.Location = new System.Drawing.Point(101, 115);
            this.txtSE.Margin = new System.Windows.Forms.Padding(2);
            this.txtSE.Name = "txtSE";
            this.txtSE.Size = new System.Drawing.Size(420, 23);
            this.txtSE.TabIndex = 1;
            this.txtSE.Validating += new System.ComponentModel.CancelEventHandler(this.txtSE_Validating);
            // 
            // lblSE
            // 
            this.lblSE.AutoSize = true;
            this.lblSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSE.Location = new System.Drawing.Point(6, 117);
            this.lblSE.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSE.Name = "lblSE";
            this.lblSE.Size = new System.Drawing.Size(42, 17);
            this.lblSE.TabIndex = 13;
            this.lblSE.Text = "Email";
            // 
            // lblSD
            // 
            this.lblSD.AutoSize = true;
            this.lblSD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSD.Location = new System.Drawing.Point(6, 147);
            this.lblSD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSD.Name = "lblSD";
            this.lblSD.Size = new System.Drawing.Size(48, 17);
            this.lblSD.TabIndex = 12;
            this.lblSD.Text = "D-O-B";
            // 
            // txTSC
            // 
            this.txTSC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txTSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txTSC.Location = new System.Drawing.Point(101, 85);
            this.txTSC.Margin = new System.Windows.Forms.Padding(2);
            this.txTSC.Name = "txTSC";
            this.txTSC.Size = new System.Drawing.Size(420, 23);
            this.txTSC.TabIndex = 11;
            // 
            // txtSL
            // 
            this.txtSL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSL.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSL.Location = new System.Drawing.Point(101, 53);
            this.txtSL.Margin = new System.Windows.Forms.Padding(2);
            this.txtSL.Name = "txtSL";
            this.txtSL.Size = new System.Drawing.Size(420, 23);
            this.txtSL.TabIndex = 10;
            // 
            // txtSF
            // 
            this.txtSF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSF.Location = new System.Drawing.Point(101, 23);
            this.txtSF.Margin = new System.Windows.Forms.Padding(2);
            this.txtSF.Name = "txtSF";
            this.txtSF.Size = new System.Drawing.Size(420, 23);
            this.txtSF.TabIndex = 0;
            this.txtSF.TextChanged += new System.EventHandler(this.txtSF_TextChanged);
            this.txtSF.Leave += new System.EventHandler(this.txtSF_Leave);
            this.txtSF.Validating += new System.ComponentModel.CancelEventHandler(this.txtSF_Validating);
            // 
            // lblSLN
            // 
            this.lblSLN.AutoSize = true;
            this.lblSLN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSLN.Location = new System.Drawing.Point(6, 55);
            this.lblSLN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSLN.Name = "lblSLN";
            this.lblSLN.Size = new System.Drawing.Size(76, 17);
            this.lblSLN.TabIndex = 8;
            this.lblSLN.Text = "Last Name";
            // 
            // lblSC
            // 
            this.lblSC.AutoSize = true;
            this.lblSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSC.Location = new System.Drawing.Point(6, 88);
            this.lblSC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSC.Name = "lblSC";
            this.lblSC.Size = new System.Drawing.Size(56, 17);
            this.lblSC.TabIndex = 7;
            this.lblSC.Text = "Contact";
            // 
            // lblSNam
            // 
            this.lblSNam.AutoSize = true;
            this.lblSNam.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSNam.Location = new System.Drawing.Point(6, 25);
            this.lblSNam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSNam.Name = "lblSNam";
            this.lblSNam.Size = new System.Drawing.Size(76, 17);
            this.lblSNam.TabIndex = 6;
            this.lblSNam.Text = "First Name";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.46729F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.53271F));
            this.tableLayoutPanel4.Controls.Add(this.btnSU, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnSC, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblSSR, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtSRS, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnSs, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(568, 2);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(214, 250);
            this.tableLayoutPanel4.TabIndex = 11111;
            // 
            // btnSU
            // 
            this.btnSU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSU.Location = new System.Drawing.Point(124, 24);
            this.btnSU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnSU.Name = "btnSU";
            this.btnSU.Size = new System.Drawing.Size(74, 37);
            this.btnSU.TabIndex = 12;
            this.btnSU.TabStop = false;
            this.btnSU.Text = "LOAD";
            this.btnSU.UseVisualStyleBackColor = true;
            this.btnSU.Click += new System.EventHandler(this.btnSU_Click);
            // 
            // btnSC
            // 
            this.btnSC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSC.Location = new System.Drawing.Point(16, 23);
            this.btnSC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnSC.Name = "btnSC";
            this.btnSC.Size = new System.Drawing.Size(76, 39);
            this.btnSC.TabIndex = 10;
            this.btnSC.TabStop = false;
            this.btnSC.Text = "Create";
            this.btnSC.UseVisualStyleBackColor = true;
            this.btnSC.Click += new System.EventHandler(this.btnSC_Click);
            // 
            // lblSSR
            // 
            this.lblSSR.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSSR.AutoSize = true;
            this.lblSSR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblSSR.Location = new System.Drawing.Point(8, 114);
            this.lblSSR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSSR.Name = "lblSSR";
            this.lblSSR.Size = new System.Drawing.Size(92, 17);
            this.lblSSR.TabIndex = 18;
            this.lblSSR.Text = "Reg. Number";
            // 
            // txtSRS
            // 
            this.txtSRS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSRS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSRS.Location = new System.Drawing.Point(110, 111);
            this.txtSRS.Margin = new System.Windows.Forms.Padding(2);
            this.txtSRS.Name = "txtSRS";
            this.txtSRS.Size = new System.Drawing.Size(102, 23);
            this.txtSRS.TabIndex = 19;
            // 
            // btnSs
            // 
            this.btnSs.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnSs.Location = new System.Drawing.Point(123, 185);
            this.btnSs.Margin = new System.Windows.Forms.Padding(0);
            this.btnSs.Name = "btnSs";
            this.btnSs.Size = new System.Drawing.Size(76, 43);
            this.btnSs.TabIndex = 13;
            this.btnSs.TabStop = false;
            this.btnSs.Text = "Search";
            this.btnSs.UseVisualStyleBackColor = true;
            this.btnSs.Click += new System.EventHandler(this.btnSs_Click);
            // 
            // StudentGV
            // 
            this.StudentGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StudentGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StudentGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EDIT});
            this.StudentGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StudentGV.Location = new System.Drawing.Point(8, 266);
            this.StudentGV.Margin = new System.Windows.Forms.Padding(8);
            this.StudentGV.Name = "StudentGV";
            this.StudentGV.RowHeadersWidth = 51;
            this.StudentGV.RowTemplate.Height = 24;
            this.StudentGV.Size = new System.Drawing.Size(772, 146);
            this.StudentGV.TabIndex = 10765;
            this.StudentGV.TabStop = false;
            this.StudentGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.StudentGV_CellContentClick_1);
            // 
            // EDIT
            // 
            this.EDIT.HeaderText = "EDIT";
            this.EDIT.Name = "EDIT";
            this.EDIT.Text = "EDIT";
            this.EDIT.ToolTipText = "EDIT";
            this.EDIT.UseColumnTextForButtonValue = true;
            // 
            // LandingPage
            // 
            this.LandingPage.BackgroundImage = global::FYP.Properties.Resources._4;
            this.LandingPage.Controls.Add(this.tableLayoutPanel1);
            this.LandingPage.Location = new System.Drawing.Point(4, 22);
            this.LandingPage.Name = "LandingPage";
            this.LandingPage.Padding = new System.Windows.Forms.Padding(3);
            this.LandingPage.Size = new System.Drawing.Size(792, 424);
            this.LandingPage.TabIndex = 0;
            this.LandingPage.Text = "LandingPage";
            this.LandingPage.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.homepagelabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(786, 418);
            this.tableLayoutPanel1.TabIndex = 1001;
            // 
            // homepagelabel
            // 
            this.homepagelabel.AutoSize = true;
            this.homepagelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homepagelabel.Font = new System.Drawing.Font("Segoe Print", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homepagelabel.ForeColor = System.Drawing.Color.Black;
            this.homepagelabel.Location = new System.Drawing.Point(23, 110);
            this.homepagelabel.Name = "homepagelabel";
            this.homepagelabel.Size = new System.Drawing.Size(740, 93);
            this.homepagelabel.TabIndex = 11111;
            this.homepagelabel.Text = "FYP Management System";
            this.homepagelabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.ForeColor = System.Drawing.Color.White;
            this.progressBar1.Location = new System.Drawing.Point(23, 258);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 15, 3, 15);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(740, 16);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 11111;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // tablelayout
            // 
            this.tablelayout.Controls.Add(this.LandingPage);
            this.tablelayout.Controls.Add(this.TPStudent);
            this.tablelayout.Controls.Add(this.tAdvsior);
            this.tablelayout.Controls.Add(this.Project);
            this.tablelayout.Controls.Add(this.tbProjectAdvsior);
            this.tablelayout.Controls.Add(this.tbCreateGroups);
            this.tablelayout.Controls.Add(this.tbEvalutions);
            this.tablelayout.Controls.Add(this.tbGroupEvalutions);
            this.tablelayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablelayout.Location = new System.Drawing.Point(0, 0);
            this.tablelayout.Name = "tablelayout";
            this.tablelayout.SelectedIndex = 0;
            this.tablelayout.Size = new System.Drawing.Size(800, 450);
            this.tablelayout.TabIndex = 1000;
            this.tablelayout.TabStop = false;
            this.tablelayout.ContextMenuStripChanged += new System.EventHandler(this.tablelayout_ContextMenuStripChanged);
            this.tablelayout.Click += new System.EventHandler(this.tablelayout_Click);
            // 
            // tbProjectAdvsior
            // 
            this.tbProjectAdvsior.Controls.Add(this.tableLayoutPanel11);
            this.tbProjectAdvsior.Location = new System.Drawing.Point(4, 22);
            this.tbProjectAdvsior.Name = "tbProjectAdvsior";
            this.tbProjectAdvsior.Padding = new System.Windows.Forms.Padding(3);
            this.tbProjectAdvsior.Size = new System.Drawing.Size(792, 424);
            this.tbProjectAdvsior.TabIndex = 7;
            this.tbProjectAdvsior.Text = "Project Adsior";
            this.tbProjectAdvsior.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.dataGridViewPA, 0, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(786, 418);
            this.tableLayoutPanel11.TabIndex = 1003;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel12.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(782, 165);
            this.tableLayoutPanel12.TabIndex = 1004;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.comboIA);
            this.panel4.Controls.Add(this.IndustrialAdvsior);
            this.panel4.Controls.Add(this.comboCoAd);
            this.panel4.Controls.Add(this.CoAdvsior);
            this.panel4.Controls.Add(this.dateTimePickerPA);
            this.panel4.Controls.Add(this.comPAAI);
            this.panel4.Controls.Add(this.comPAPI);
            this.panel4.Controls.Add(this.lblPAD);
            this.panel4.Controls.Add(this.lblPAPI);
            this.panel4.Controls.Add(this.MainAdvsior);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(2, 2);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(560, 161);
            this.panel4.TabIndex = 11111;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // comboIA
            // 
            this.comboIA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboIA.FormattingEnabled = true;
            this.comboIA.Location = new System.Drawing.Point(461, 67);
            this.comboIA.Name = "comboIA";
            this.comboIA.Size = new System.Drawing.Size(86, 21);
            this.comboIA.TabIndex = 29;
            this.comboIA.Click += new System.EventHandler(this.comboIA_Click);
            // 
            // IndustrialAdvsior
            // 
            this.IndustrialAdvsior.AutoSize = true;
            this.IndustrialAdvsior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.IndustrialAdvsior.Location = new System.Drawing.Point(369, 68);
            this.IndustrialAdvsior.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.IndustrialAdvsior.Name = "IndustrialAdvsior";
            this.IndustrialAdvsior.Size = new System.Drawing.Size(78, 17);
            this.IndustrialAdvsior.TabIndex = 28;
            this.IndustrialAdvsior.Text = "Ind Advsior";
            // 
            // comboCoAd
            // 
            this.comboCoAd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboCoAd.FormattingEnabled = true;
            this.comboCoAd.Location = new System.Drawing.Point(264, 67);
            this.comboCoAd.Name = "comboCoAd";
            this.comboCoAd.Size = new System.Drawing.Size(86, 21);
            this.comboCoAd.TabIndex = 27;
            this.comboCoAd.Click += new System.EventHandler(this.comboCoAd_Click);
            // 
            // CoAdvsior
            // 
            this.CoAdvsior.AutoSize = true;
            this.CoAdvsior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.CoAdvsior.Location = new System.Drawing.Point(183, 71);
            this.CoAdvsior.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.CoAdvsior.Name = "CoAdvsior";
            this.CoAdvsior.Size = new System.Drawing.Size(76, 17);
            this.CoAdvsior.TabIndex = 26;
            this.CoAdvsior.Text = "Co Advsior";
            // 
            // dateTimePickerPA
            // 
            this.dateTimePickerPA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerPA.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerPA.Location = new System.Drawing.Point(126, 123);
            this.dateTimePickerPA.Name = "dateTimePickerPA";
            this.dateTimePickerPA.Size = new System.Drawing.Size(362, 20);
            this.dateTimePickerPA.TabIndex = 25;
            // 
            // comPAAI
            // 
            this.comPAAI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comPAAI.FormattingEnabled = true;
            this.comPAAI.Location = new System.Drawing.Point(100, 67);
            this.comPAAI.Name = "comPAAI";
            this.comPAAI.Size = new System.Drawing.Size(78, 21);
            this.comPAAI.TabIndex = 24;
            this.comPAAI.SelectedIndexChanged += new System.EventHandler(this.comPAAI_SelectedIndexChanged);
            this.comPAAI.Click += new System.EventHandler(this.comPAAI_Click);
            // 
            // comPAPI
            // 
            this.comPAPI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comPAPI.FormattingEnabled = true;
            this.comPAPI.Location = new System.Drawing.Point(124, 26);
            this.comPAPI.Name = "comPAPI";
            this.comPAPI.Size = new System.Drawing.Size(364, 21);
            this.comPAPI.TabIndex = 23;
            this.comPAPI.SelectedIndexChanged += new System.EventHandler(this.comPAPI_SelectedIndexChanged);
            this.comPAPI.Click += new System.EventHandler(this.comPAPI_Click);
            // 
            // lblPAD
            // 
            this.lblPAD.AutoSize = true;
            this.lblPAD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPAD.Location = new System.Drawing.Point(6, 127);
            this.lblPAD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPAD.Name = "lblPAD";
            this.lblPAD.Size = new System.Drawing.Size(115, 17);
            this.lblPAD.TabIndex = 12;
            this.lblPAD.Text = "Assignment Date";
            // 
            // lblPAPI
            // 
            this.lblPAPI.AutoSize = true;
            this.lblPAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPAPI.Location = new System.Drawing.Point(6, 26);
            this.lblPAPI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPAPI.Name = "lblPAPI";
            this.lblPAPI.Size = new System.Drawing.Size(69, 17);
            this.lblPAPI.TabIndex = 8;
            this.lblPAPI.Text = "Project ID";
            // 
            // MainAdvsior
            // 
            this.MainAdvsior.AutoSize = true;
            this.MainAdvsior.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.MainAdvsior.Location = new System.Drawing.Point(6, 68);
            this.MainAdvsior.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MainAdvsior.Name = "MainAdvsior";
            this.MainAdvsior.Size = new System.Drawing.Size(89, 17);
            this.MainAdvsior.TabIndex = 6;
            this.MainAdvsior.Text = "Main Advsior";
            this.MainAdvsior.Click += new System.EventHandler(this.MainAdvsior_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.btnPAU, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.btnPAC, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.lblPAPIS, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.txtPAPI, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.btnPAS, 1, 2);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 3;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(214, 161);
            this.tableLayoutPanel13.TabIndex = 1000;
            this.tableLayoutPanel13.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel13_Paint);
            // 
            // btnPAU
            // 
            this.btnPAU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPAU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPAU.Location = new System.Drawing.Point(123, 16);
            this.btnPAU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnPAU.Name = "btnPAU";
            this.btnPAU.Size = new System.Drawing.Size(74, 31);
            this.btnPAU.TabIndex = 12;
            this.btnPAU.Text = "Reload";
            this.btnPAU.UseVisualStyleBackColor = true;
            this.btnPAU.Click += new System.EventHandler(this.btnPAU_Click);
            // 
            // btnPAC
            // 
            this.btnPAC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPAC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPAC.Location = new System.Drawing.Point(15, 16);
            this.btnPAC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnPAC.Name = "btnPAC";
            this.btnPAC.Size = new System.Drawing.Size(76, 31);
            this.btnPAC.TabIndex = 10;
            this.btnPAC.Text = "Create";
            this.btnPAC.UseVisualStyleBackColor = true;
            this.btnPAC.Click += new System.EventHandler(this.btnPAC_Click);
            // 
            // lblPAPIS
            // 
            this.lblPAPIS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPAPIS.AutoSize = true;
            this.lblPAPIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblPAPIS.Location = new System.Drawing.Point(19, 80);
            this.lblPAPIS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPAPIS.Name = "lblPAPIS";
            this.lblPAPIS.Size = new System.Drawing.Size(69, 17);
            this.lblPAPIS.TabIndex = 18;
            this.lblPAPIS.Text = "Project ID";
            // 
            // txtPAPI
            // 
            this.txtPAPI.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtPAPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtPAPI.Location = new System.Drawing.Point(109, 77);
            this.txtPAPI.Margin = new System.Windows.Forms.Padding(2);
            this.txtPAPI.Name = "txtPAPI";
            this.txtPAPI.Size = new System.Drawing.Size(103, 23);
            this.txtPAPI.TabIndex = 19;
            // 
            // btnPAS
            // 
            this.btnPAS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPAS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnPAS.Location = new System.Drawing.Point(116, 127);
            this.btnPAS.Margin = new System.Windows.Forms.Padding(0);
            this.btnPAS.Name = "btnPAS";
            this.btnPAS.Size = new System.Drawing.Size(89, 24);
            this.btnPAS.TabIndex = 20;
            this.btnPAS.Text = "Search";
            this.btnPAS.UseVisualStyleBackColor = true;
            this.btnPAS.Click += new System.EventHandler(this.btnPAS_Click);
            // 
            // dataGridViewPA
            // 
            this.dataGridViewPA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPA.Location = new System.Drawing.Point(8, 177);
            this.dataGridViewPA.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridViewPA.Name = "dataGridViewPA";
            this.dataGridViewPA.RowHeadersWidth = 51;
            this.dataGridViewPA.RowTemplate.Height = 24;
            this.dataGridViewPA.Size = new System.Drawing.Size(770, 233);
            this.dataGridViewPA.TabIndex = 11111;
            this.dataGridViewPA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPA_CellContentClick);
            // 
            // tbCreateGroups
            // 
            this.tbCreateGroups.Controls.Add(this.tableLayoutPanel17);
            this.tbCreateGroups.Controls.Add(this.tableLayoutPanel14);
            this.tbCreateGroups.Location = new System.Drawing.Point(4, 22);
            this.tbCreateGroups.Name = "tbCreateGroups";
            this.tbCreateGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tbCreateGroups.Size = new System.Drawing.Size(792, 424);
            this.tbCreateGroups.TabIndex = 9;
            this.tbCreateGroups.Text = "Create Groups";
            this.tbCreateGroups.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 1;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel18, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.dataGridViewGP, 0, 1);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 164);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(786, 257);
            this.tableLayoutPanel17.TabIndex = 21111;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel18.Controls.Add(this.panel6, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.tableLayoutPanel19, 1, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(782, 118);
            this.tableLayoutPanel18.TabIndex = 11111;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dateTimePickerGA);
            this.panel6.Controls.Add(this.comGPGI);
            this.panel6.Controls.Add(this.comGPPI);
            this.panel6.Controls.Add(this.lblGPAD);
            this.panel6.Controls.Add(this.lblGPPI);
            this.panel6.Controls.Add(this.lblAGPGI);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(2, 2);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(560, 114);
            this.panel6.TabIndex = 11111;
            // 
            // dateTimePickerGA
            // 
            this.dateTimePickerGA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerGA.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerGA.Location = new System.Drawing.Point(123, 69);
            this.dateTimePickerGA.Name = "dateTimePickerGA";
            this.dateTimePickerGA.Size = new System.Drawing.Size(361, 20);
            this.dateTimePickerGA.TabIndex = 25;
            // 
            // comGPGI
            // 
            this.comGPGI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comGPGI.FormattingEnabled = true;
            this.comGPGI.Location = new System.Drawing.Point(121, 14);
            this.comGPGI.Name = "comGPGI";
            this.comGPGI.Size = new System.Drawing.Size(363, 21);
            this.comGPGI.TabIndex = 24;
            this.comGPGI.SelectedIndexChanged += new System.EventHandler(this.comGPGI_SelectedIndexChanged);
            this.comGPGI.Click += new System.EventHandler(this.comGPGI_Click);
            // 
            // comGPPI
            // 
            this.comGPPI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comGPPI.FormattingEnabled = true;
            this.comGPPI.Location = new System.Drawing.Point(121, 39);
            this.comGPPI.Name = "comGPPI";
            this.comGPPI.Size = new System.Drawing.Size(363, 21);
            this.comGPPI.TabIndex = 23;
            this.comGPPI.Click += new System.EventHandler(this.comGPPI_Click);
            // 
            // lblGPAD
            // 
            this.lblGPAD.AutoSize = true;
            this.lblGPAD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGPAD.Location = new System.Drawing.Point(2, 72);
            this.lblGPAD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGPAD.Name = "lblGPAD";
            this.lblGPAD.Size = new System.Drawing.Size(115, 17);
            this.lblGPAD.TabIndex = 12;
            this.lblGPAD.Text = "Assignment Date";
            // 
            // lblGPPI
            // 
            this.lblGPPI.AutoSize = true;
            this.lblGPPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGPPI.Location = new System.Drawing.Point(6, 40);
            this.lblGPPI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGPPI.Name = "lblGPPI";
            this.lblGPPI.Size = new System.Drawing.Size(66, 17);
            this.lblGPPI.TabIndex = 8;
            this.lblGPPI.Text = "Studet ID";
            this.lblGPPI.Click += new System.EventHandler(this.label21_Click);
            // 
            // lblAGPGI
            // 
            this.lblAGPGI.AutoSize = true;
            this.lblAGPGI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblAGPGI.Location = new System.Drawing.Point(7, 13);
            this.lblAGPGI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAGPGI.Name = "lblAGPGI";
            this.lblAGPGI.Size = new System.Drawing.Size(65, 17);
            this.lblAGPGI.TabIndex = 6;
            this.lblAGPGI.Text = "Group ID";
            this.lblAGPGI.Click += new System.EventHandler(this.label23_Click);
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.btnGPU, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.btnGPC, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.txtSerchStudentGroups, 1, 1);
            this.tableLayoutPanel19.Controls.Add(this.btnCGS, 1, 2);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 3;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(214, 114);
            this.tableLayoutPanel19.TabIndex = 11111;
            // 
            // btnGPU
            // 
            this.btnGPU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGPU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGPU.Location = new System.Drawing.Point(123, 8);
            this.btnGPU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnGPU.Name = "btnGPU";
            this.btnGPU.Size = new System.Drawing.Size(74, 25);
            this.btnGPU.TabIndex = 12;
            this.btnGPU.Text = "Load";
            this.btnGPU.UseVisualStyleBackColor = true;
            this.btnGPU.Click += new System.EventHandler(this.btnGPU_Click);
            // 
            // btnGPC
            // 
            this.btnGPC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGPC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGPC.Location = new System.Drawing.Point(15, 8);
            this.btnGPC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnGPC.Name = "btnGPC";
            this.btnGPC.Size = new System.Drawing.Size(76, 25);
            this.btnGPC.TabIndex = 10;
            this.btnGPC.Text = "Create";
            this.btnGPC.UseVisualStyleBackColor = true;
            this.btnGPC.Click += new System.EventHandler(this.btnGPC_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(16, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Student ID";
            // 
            // txtSerchStudentGroups
            // 
            this.txtSerchStudentGroups.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSerchStudentGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtSerchStudentGroups.Location = new System.Drawing.Point(109, 44);
            this.txtSerchStudentGroups.Margin = new System.Windows.Forms.Padding(2);
            this.txtSerchStudentGroups.Name = "txtSerchStudentGroups";
            this.txtSerchStudentGroups.Size = new System.Drawing.Size(103, 23);
            this.txtSerchStudentGroups.TabIndex = 20;
            this.txtSerchStudentGroups.TextChanged += new System.EventHandler(this.txtSerchStudentGroups_TextChanged);
            // 
            // btnCGS
            // 
            this.btnCGS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCGS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnCGS.Location = new System.Drawing.Point(116, 82);
            this.btnCGS.Margin = new System.Windows.Forms.Padding(0);
            this.btnCGS.Name = "btnCGS";
            this.btnCGS.Size = new System.Drawing.Size(89, 24);
            this.btnCGS.TabIndex = 21;
            this.btnCGS.Text = "Search";
            this.btnCGS.UseVisualStyleBackColor = true;
            this.btnCGS.Click += new System.EventHandler(this.btnCGS_Click);
            // 
            // dataGridViewGP
            // 
            this.dataGridViewGP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeactivateStudent});
            this.dataGridViewGP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGP.Location = new System.Drawing.Point(8, 130);
            this.dataGridViewGP.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridViewGP.Name = "dataGridViewGP";
            this.dataGridViewGP.RowHeadersWidth = 51;
            this.dataGridViewGP.RowTemplate.Height = 24;
            this.dataGridViewGP.Size = new System.Drawing.Size(770, 119);
            this.dataGridViewGP.TabIndex = 11111;
            this.dataGridViewGP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGP_CellContentClick);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel15, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.dataGridViewGC, 0, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(786, 161);
            this.tableLayoutPanel14.TabIndex = 1001;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel15.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.tableLayoutPanel16, 1, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(782, 57);
            this.tableLayoutPanel15.TabIndex = 11111;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dateTimePickerGC);
            this.panel5.Controls.Add(this.lblGC);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(2, 2);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(560, 53);
            this.panel5.TabIndex = 11111;
            // 
            // dateTimePickerGC
            // 
            this.dateTimePickerGC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerGC.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerGC.Location = new System.Drawing.Point(102, 17);
            this.dateTimePickerGC.Name = "dateTimePickerGC";
            this.dateTimePickerGC.Size = new System.Drawing.Size(458, 20);
            this.dateTimePickerGC.TabIndex = 24;
            // 
            // lblGC
            // 
            this.lblGC.AutoSize = true;
            this.lblGC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGC.Location = new System.Drawing.Point(2, 20);
            this.lblGC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGC.Name = "lblGC";
            this.lblGC.Size = new System.Drawing.Size(83, 17);
            this.lblGC.TabIndex = 6;
            this.lblGC.Text = "Created ON";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel16.Controls.Add(this.btnGCU, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.btnGC, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(214, 53);
            this.tableLayoutPanel16.TabIndex = 11111;
            // 
            // btnGCU
            // 
            this.btnGCU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGCU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGCU.Location = new System.Drawing.Point(129, 13);
            this.btnGCU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnGCU.Name = "btnGCU";
            this.btnGCU.Size = new System.Drawing.Size(70, 31);
            this.btnGCU.TabIndex = 11;
            this.btnGCU.Text = "LOAD";
            this.btnGCU.UseVisualStyleBackColor = true;
            this.btnGCU.Click += new System.EventHandler(this.btnGCU_Click);
            // 
            // btnGC
            // 
            this.btnGC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGC.Location = new System.Drawing.Point(19, 13);
            this.btnGC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnGC.Name = "btnGC";
            this.btnGC.Size = new System.Drawing.Size(76, 31);
            this.btnGC.TabIndex = 10;
            this.btnGC.Text = "Create";
            this.btnGC.UseVisualStyleBackColor = true;
            this.btnGC.Click += new System.EventHandler(this.btnGC_Click);
            // 
            // dataGridViewGC
            // 
            this.dataGridViewGC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewGC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGC.Location = new System.Drawing.Point(8, 69);
            this.dataGridViewGC.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridViewGC.Name = "dataGridViewGC";
            this.dataGridViewGC.RowHeadersWidth = 51;
            this.dataGridViewGC.RowTemplate.Height = 24;
            this.dataGridViewGC.Size = new System.Drawing.Size(770, 84);
            this.dataGridViewGC.TabIndex = 11111;
            // 
            // tbEvalutions
            // 
            this.tbEvalutions.Controls.Add(this.tableLayoutPanel20);
            this.tbEvalutions.Location = new System.Drawing.Point(4, 22);
            this.tbEvalutions.Name = "tbEvalutions";
            this.tbEvalutions.Padding = new System.Windows.Forms.Padding(3);
            this.tbEvalutions.Size = new System.Drawing.Size(792, 424);
            this.tbEvalutions.TabIndex = 10;
            this.tbEvalutions.Text = "Evaluations";
            this.tbEvalutions.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel21, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.dataGridViewE, 0, 1);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(786, 418);
            this.tableLayoutPanel20.TabIndex = 1111;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel21.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel22, 1, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(782, 165);
            this.tableLayoutPanel21.TabIndex = 11111;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtTW);
            this.panel7.Controls.Add(this.txtTM);
            this.panel7.Controls.Add(this.txtEN);
            this.panel7.Controls.Add(this.lblETW);
            this.panel7.Controls.Add(this.lblET);
            this.panel7.Controls.Add(this.lblEN);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(2, 2);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(560, 161);
            this.panel7.TabIndex = 11111;
            // 
            // txtTW
            // 
            this.txtTW.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTW.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTW.Location = new System.Drawing.Point(122, 81);
            this.txtTW.Margin = new System.Windows.Forms.Padding(2);
            this.txtTW.Name = "txtTW";
            this.txtTW.Size = new System.Drawing.Size(331, 23);
            this.txtTW.TabIndex = 26;
            // 
            // txtTM
            // 
            this.txtTM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTM.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtTM.Location = new System.Drawing.Point(124, 50);
            this.txtTM.Margin = new System.Windows.Forms.Padding(2);
            this.txtTM.Name = "txtTM";
            this.txtTM.Size = new System.Drawing.Size(329, 23);
            this.txtTM.TabIndex = 25;
            // 
            // txtEN
            // 
            this.txtEN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtEN.Location = new System.Drawing.Point(124, 20);
            this.txtEN.Margin = new System.Windows.Forms.Padding(2);
            this.txtEN.Name = "txtEN";
            this.txtEN.Size = new System.Drawing.Size(329, 23);
            this.txtEN.TabIndex = 24;
            // 
            // lblETW
            // 
            this.lblETW.AutoSize = true;
            this.lblETW.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblETW.Location = new System.Drawing.Point(6, 84);
            this.lblETW.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblETW.Name = "lblETW";
            this.lblETW.Size = new System.Drawing.Size(112, 17);
            this.lblETW.TabIndex = 11;
            this.lblETW.Text = "Total Wieghtage";
            // 
            // lblET
            // 
            this.lblET.AutoSize = true;
            this.lblET.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblET.Location = new System.Drawing.Point(6, 56);
            this.lblET.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblET.Name = "lblET";
            this.lblET.Size = new System.Drawing.Size(82, 17);
            this.lblET.TabIndex = 8;
            this.lblET.Text = "Total Marks";
            // 
            // lblEN
            // 
            this.lblEN.AutoSize = true;
            this.lblEN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblEN.Location = new System.Drawing.Point(6, 26);
            this.lblEN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEN.Name = "lblEN";
            this.lblEN.Size = new System.Drawing.Size(45, 17);
            this.lblEN.TabIndex = 6;
            this.lblEN.Text = "Name";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.btnEU, 1, 0);
            this.tableLayoutPanel22.Controls.Add(this.btnEA, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.lblENS, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.txtENS, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.btnES, 1, 2);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 3;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(214, 161);
            this.tableLayoutPanel22.TabIndex = 11111;
            // 
            // btnEU
            // 
            this.btnEU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnEU.Location = new System.Drawing.Point(123, 16);
            this.btnEU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnEU.Name = "btnEU";
            this.btnEU.Size = new System.Drawing.Size(74, 31);
            this.btnEU.TabIndex = 12;
            this.btnEU.Text = "Load";
            this.btnEU.UseVisualStyleBackColor = true;
            this.btnEU.Click += new System.EventHandler(this.btnEU_Click);
            // 
            // btnEA
            // 
            this.btnEA.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEA.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnEA.Location = new System.Drawing.Point(15, 16);
            this.btnEA.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnEA.Name = "btnEA";
            this.btnEA.Size = new System.Drawing.Size(76, 31);
            this.btnEA.TabIndex = 10;
            this.btnEA.Text = "Create";
            this.btnEA.UseVisualStyleBackColor = true;
            this.btnEA.Click += new System.EventHandler(this.btnEA_Click);
            // 
            // lblENS
            // 
            this.lblENS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblENS.AutoSize = true;
            this.lblENS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblENS.Location = new System.Drawing.Point(31, 80);
            this.lblENS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblENS.Name = "lblENS";
            this.lblENS.Size = new System.Drawing.Size(45, 17);
            this.lblENS.TabIndex = 18;
            this.lblENS.Text = "Name";
            // 
            // txtENS
            // 
            this.txtENS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtENS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtENS.Location = new System.Drawing.Point(109, 77);
            this.txtENS.Margin = new System.Windows.Forms.Padding(2);
            this.txtENS.Name = "txtENS";
            this.txtENS.Size = new System.Drawing.Size(103, 23);
            this.txtENS.TabIndex = 19;
            // 
            // btnES
            // 
            this.btnES.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnES.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnES.Location = new System.Drawing.Point(116, 127);
            this.btnES.Margin = new System.Windows.Forms.Padding(0);
            this.btnES.Name = "btnES";
            this.btnES.Size = new System.Drawing.Size(89, 24);
            this.btnES.TabIndex = 13;
            this.btnES.Text = "Search";
            this.btnES.UseVisualStyleBackColor = true;
            this.btnES.Click += new System.EventHandler(this.btnES_Click);
            // 
            // dataGridViewE
            // 
            this.dataGridViewE.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewE.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditEvaultions});
            this.dataGridViewE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewE.Location = new System.Drawing.Point(8, 177);
            this.dataGridViewE.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridViewE.Name = "dataGridViewE";
            this.dataGridViewE.RowHeadersWidth = 51;
            this.dataGridViewE.RowTemplate.Height = 24;
            this.dataGridViewE.Size = new System.Drawing.Size(770, 233);
            this.dataGridViewE.TabIndex = 11111;
            this.dataGridViewE.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewE_CellContentClick);
            // 
            // EditEvaultions
            // 
            this.EditEvaultions.HeaderText = "EditEvaultions";
            this.EditEvaultions.Name = "EditEvaultions";
            this.EditEvaultions.Text = "EditEvaultions";
            this.EditEvaultions.UseColumnTextForButtonValue = true;
            // 
            // tbGroupEvalutions
            // 
            this.tbGroupEvalutions.Controls.Add(this.tableLayoutPanel23);
            this.tbGroupEvalutions.Location = new System.Drawing.Point(4, 22);
            this.tbGroupEvalutions.Name = "tbGroupEvalutions";
            this.tbGroupEvalutions.Padding = new System.Windows.Forms.Padding(3);
            this.tbGroupEvalutions.Size = new System.Drawing.Size(792, 424);
            this.tbGroupEvalutions.TabIndex = 11;
            this.tbGroupEvalutions.Text = "Group Evalutions";
            this.tbGroupEvalutions.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Controls.Add(this.tableLayoutPanel24, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.dataGridViewGE, 0, 1);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(786, 418);
            this.tableLayoutPanel23.TabIndex = 11111;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel24.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.tableLayoutPanel25, 1, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel24.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(782, 165);
            this.tableLayoutPanel24.TabIndex = 111111;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtGEOM);
            this.panel8.Controls.Add(this.dateTimePickerED);
            this.panel8.Controls.Add(this.comGEGI);
            this.panel8.Controls.Add(this.comGEEI);
            this.panel8.Controls.Add(this.lblGEED);
            this.panel8.Controls.Add(this.lblGEOM);
            this.panel8.Controls.Add(this.lblGEEI);
            this.panel8.Controls.Add(this.lblGEGI);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(2, 2);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(560, 161);
            this.panel8.TabIndex = 11111;
            // 
            // dateTimePickerED
            // 
            this.dateTimePickerED.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePickerED.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerED.Location = new System.Drawing.Point(119, 113);
            this.dateTimePickerED.Name = "dateTimePickerED";
            this.dateTimePickerED.Size = new System.Drawing.Size(392, 20);
            this.dateTimePickerED.TabIndex = 25;
            // 
            // comGEGI
            // 
            this.comGEGI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comGEGI.FormattingEnabled = true;
            this.comGEGI.Location = new System.Drawing.Point(119, 25);
            this.comGEGI.Name = "comGEGI";
            this.comGEGI.Size = new System.Drawing.Size(392, 21);
            this.comGEGI.TabIndex = 24;
            this.comGEGI.Click += new System.EventHandler(this.comGEGI_Click);
            // 
            // comGEEI
            // 
            this.comGEEI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comGEEI.FormattingEnabled = true;
            this.comGEEI.Location = new System.Drawing.Point(119, 60);
            this.comGEEI.Name = "comGEEI";
            this.comGEEI.Size = new System.Drawing.Size(392, 21);
            this.comGEEI.TabIndex = 23;
            this.comGEEI.SelectedIndexChanged += new System.EventHandler(this.comGEEI_SelectedIndexChanged);
            this.comGEEI.Click += new System.EventHandler(this.comGEEI_Click);
            // 
            // lblGEED
            // 
            this.lblGEED.AutoSize = true;
            this.lblGEED.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGEED.Location = new System.Drawing.Point(6, 113);
            this.lblGEED.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGEED.Name = "lblGEED";
            this.lblGEED.Size = new System.Drawing.Size(100, 17);
            this.lblGEED.TabIndex = 12;
            this.lblGEED.Text = "Evalution Date";
            // 
            // lblGEOM
            // 
            this.lblGEOM.AutoSize = true;
            this.lblGEOM.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGEOM.Location = new System.Drawing.Point(6, 84);
            this.lblGEOM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGEOM.Name = "lblGEOM";
            this.lblGEOM.Size = new System.Drawing.Size(108, 17);
            this.lblGEOM.TabIndex = 11;
            this.lblGEOM.Text = "Obtained Marks";
            // 
            // lblGEEI
            // 
            this.lblGEEI.AutoSize = true;
            this.lblGEEI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGEEI.Location = new System.Drawing.Point(6, 56);
            this.lblGEEI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGEEI.Name = "lblGEEI";
            this.lblGEEI.Size = new System.Drawing.Size(83, 17);
            this.lblGEEI.TabIndex = 8;
            this.lblGEEI.Text = "Evalution ID";
            // 
            // lblGEGI
            // 
            this.lblGEGI.AutoSize = true;
            this.lblGEGI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGEGI.Location = new System.Drawing.Point(6, 26);
            this.lblGEGI.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGEGI.Name = "lblGEGI";
            this.lblGEGI.Size = new System.Drawing.Size(65, 17);
            this.lblGEGI.TabIndex = 6;
            this.lblGEGI.Text = "Group ID";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Controls.Add(this.btnGEU, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.btnGEC, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.lblGEGIS, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.txtGEGI, 1, 1);
            this.tableLayoutPanel25.Controls.Add(this.btnGES, 1, 2);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(566, 2);
            this.tableLayoutPanel25.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 3;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(214, 161);
            this.tableLayoutPanel25.TabIndex = 11111;
            // 
            // btnGEU
            // 
            this.btnGEU.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGEU.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGEU.Location = new System.Drawing.Point(123, 12);
            this.btnGEU.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnGEU.Name = "btnGEU";
            this.btnGEU.Size = new System.Drawing.Size(74, 31);
            this.btnGEU.TabIndex = 12;
            this.btnGEU.Text = "LOAD";
            this.btnGEU.UseVisualStyleBackColor = true;
            this.btnGEU.Click += new System.EventHandler(this.btnGEU_Click);
            // 
            // btnGEC
            // 
            this.btnGEC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGEC.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGEC.Location = new System.Drawing.Point(15, 12);
            this.btnGEC.Margin = new System.Windows.Forms.Padding(15, 8, 15, 4);
            this.btnGEC.Name = "btnGEC";
            this.btnGEC.Size = new System.Drawing.Size(76, 31);
            this.btnGEC.TabIndex = 10;
            this.btnGEC.Text = "Create";
            this.btnGEC.UseVisualStyleBackColor = true;
            this.btnGEC.Click += new System.EventHandler(this.btnGEC_Click);
            // 
            // lblGEGIS
            // 
            this.lblGEGIS.AccessibleName = "";
            this.lblGEGIS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblGEGIS.AutoSize = true;
            this.lblGEGIS.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblGEGIS.Location = new System.Drawing.Point(21, 69);
            this.lblGEGIS.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGEGIS.Name = "lblGEGIS";
            this.lblGEGIS.Size = new System.Drawing.Size(65, 17);
            this.lblGEGIS.TabIndex = 18;
            this.lblGEGIS.Text = "Group ID";
            // 
            // txtGEGI
            // 
            this.txtGEGI.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtGEGI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.txtGEGI.Location = new System.Drawing.Point(109, 66);
            this.txtGEGI.Margin = new System.Windows.Forms.Padding(2);
            this.txtGEGI.Name = "txtGEGI";
            this.txtGEGI.Size = new System.Drawing.Size(103, 23);
            this.txtGEGI.TabIndex = 19;
            // 
            // btnGES
            // 
            this.btnGES.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGES.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnGES.Location = new System.Drawing.Point(116, 118);
            this.btnGES.Margin = new System.Windows.Forms.Padding(0);
            this.btnGES.Name = "btnGES";
            this.btnGES.Size = new System.Drawing.Size(89, 29);
            this.btnGES.TabIndex = 13;
            this.btnGES.Text = "Search";
            this.btnGES.UseVisualStyleBackColor = true;
            this.btnGES.Click += new System.EventHandler(this.btnGES_Click);
            // 
            // dataGridViewGE
            // 
            this.dataGridViewGE.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewGE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGE.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EditGroupEvalutions});
            this.dataGridViewGE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGE.Location = new System.Drawing.Point(8, 177);
            this.dataGridViewGE.Margin = new System.Windows.Forms.Padding(8);
            this.dataGridViewGE.Name = "dataGridViewGE";
            this.dataGridViewGE.RowHeadersWidth = 51;
            this.dataGridViewGE.RowTemplate.Height = 24;
            this.dataGridViewGE.Size = new System.Drawing.Size(770, 233);
            this.dataGridViewGE.TabIndex = 111111;
            this.dataGridViewGE.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGE_CellContentClick);
            // 
            // EditGroupEvalutions
            // 
            this.EditGroupEvalutions.HeaderText = "EditGroupEvalutions";
            this.EditGroupEvalutions.Name = "EditGroupEvalutions";
            this.EditGroupEvalutions.Text = "EditGroupEvalutions";
            this.EditGroupEvalutions.UseColumnTextForButtonValue = true;
            // 
            // errorProviderSFN
            // 
            this.errorProviderSFN.ContainerControl = this;
            // 
            // errorProviderSE
            // 
            this.errorProviderSE.ContainerControl = this;
            // 
            // errorProviderSR
            // 
            this.errorProviderSR.ContainerControl = this;
            // 
            // errorProviderdate
            // 
            this.errorProviderdate.ContainerControl = this;
            // 
            // DeactivateStudent
            // 
            this.DeactivateStudent.HeaderText = "INactive";
            this.DeactivateStudent.Name = "DeactivateStudent";
            this.DeactivateStudent.Text = "INactive";
            this.DeactivateStudent.UseColumnTextForButtonValue = true;
            // 
            // txtGEOM
            // 
            this.txtGEOM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGEOM.Location = new System.Drawing.Point(119, 87);
            this.txtGEOM.Name = "txtGEOM";
            this.txtGEOM.Size = new System.Drawing.Size(392, 20);
            this.txtGEOM.TabIndex = 27;
            this.txtGEOM.ValueChanged += new System.EventHandler(this.txtGEOM_ValueChanged);
            this.txtGEOM.Click += new System.EventHandler(this.txtGEOM_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tablelayout);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Project.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewP)).EndInit();
            this.tAdvsior.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdvsior)).EndInit();
            this.TPStudent.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentGV)).EndInit();
            this.LandingPage.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tablelayout.ResumeLayout(false);
            this.tbProjectAdvsior.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPA)).EndInit();
            this.tbCreateGroups.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGP)).EndInit();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGC)).EndInit();
            this.tbEvalutions.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewE)).EndInit();
            this.tbGroupEvalutions.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSFN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGEOM)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage Project;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtPD;
        private System.Windows.Forms.TextBox txtPT;
        private System.Windows.Forms.Label lblPD;
        private System.Windows.Forms.Label lblPT;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button btnPU;
        private System.Windows.Forms.Button btnPC;
        private System.Windows.Forms.Button btnS;
        private System.Windows.Forms.Label lblPTS;
        private System.Windows.Forms.TextBox txtPTS;
        private System.Windows.Forms.DataGridView dataGridViewP;
        private System.Windows.Forms.TabPage tAdvsior;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblAde;
        private System.Windows.Forms.ComboBox COAD;
        private System.Windows.Forms.ComboBox coAG;
        private System.Windows.Forms.TextBox txtAS;
        private System.Windows.Forms.Label lblAS;
        private System.Windows.Forms.Label lblAG;
        private System.Windows.Forms.TextBox txtAE;
        private System.Windows.Forms.Label lblAE;
        private System.Windows.Forms.Label lblAD;
        private System.Windows.Forms.TextBox txtAC;
        private System.Windows.Forms.TextBox txtAL;
        private System.Windows.Forms.TextBox txtAF;
        private System.Windows.Forms.Label lblAL;
        private System.Windows.Forms.Label lBLAC;
        private System.Windows.Forms.Label lblAF;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button btnAU;
        private System.Windows.Forms.Button btnAC;
        private System.Windows.Forms.Button btnAS;
        private System.Windows.Forms.Label lblAsearchN;
        private System.Windows.Forms.TextBox txtANS;
        private System.Windows.Forms.TabPage TPStudent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox COMSG;
        private System.Windows.Forms.DateTimePicker DATESD;
        private System.Windows.Forms.TextBox txtSR;
        private System.Windows.Forms.Label lblSR;
        private System.Windows.Forms.Label lblSG;
        private System.Windows.Forms.TextBox txtSE;
        private System.Windows.Forms.Label lblSE;
        private System.Windows.Forms.Label lblSD;
        private System.Windows.Forms.TextBox txTSC;
        private System.Windows.Forms.TextBox txtSL;
        private System.Windows.Forms.TextBox txtSF;
        private System.Windows.Forms.Label lblSLN;
        private System.Windows.Forms.Label lblSC;
        private System.Windows.Forms.Label lblSNam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnSU;
        private System.Windows.Forms.Button btnSC;
        private System.Windows.Forms.Button btnSs;
        private System.Windows.Forms.Label lblSSR;
        private System.Windows.Forms.TextBox txtSRS;
        private System.Windows.Forms.DataGridView StudentGV;
        private System.Windows.Forms.TabPage LandingPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label homepagelabel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TabControl tablelayout;
        private System.Windows.Forms.TabPage tbProjectAdvsior;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblPAD;
        private System.Windows.Forms.Label lblPAPI;
        private System.Windows.Forms.Label MainAdvsior;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btnPAU;
        private System.Windows.Forms.Button btnPAC;
        private System.Windows.Forms.TextBox txtPAPI;
        private System.Windows.Forms.DataGridView dataGridViewPA;
        private System.Windows.Forms.ComboBox comPAAI;
        private System.Windows.Forms.ComboBox comPAPI;
        private System.Windows.Forms.TabPage tbCreateGroups;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox comGPGI;
        private System.Windows.Forms.ComboBox comGPPI;
        private System.Windows.Forms.Label lblGPAD;
        private System.Windows.Forms.Label lblGPPI;
        private System.Windows.Forms.Label lblAGPGI;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Button btnGPU;
        private System.Windows.Forms.Button btnGPC;
        private System.Windows.Forms.DataGridView dataGridViewGP;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblGC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Button btnGC;
        private System.Windows.Forms.DataGridView dataGridViewGC;
        private System.Windows.Forms.TabPage tbEvalutions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtTW;
        private System.Windows.Forms.TextBox txtTM;
        private System.Windows.Forms.TextBox txtEN;
        private System.Windows.Forms.Label lblETW;
        private System.Windows.Forms.Label lblET;
        private System.Windows.Forms.Label lblEN;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.Button btnEU;
        private System.Windows.Forms.Button btnEA;
        private System.Windows.Forms.Label lblENS;
        private System.Windows.Forms.TextBox txtENS;
        private System.Windows.Forms.Button btnES;
        private System.Windows.Forms.DataGridView dataGridViewE;
        private System.Windows.Forms.TabPage tbGroupEvalutions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.ComboBox comGEGI;
        private System.Windows.Forms.ComboBox comGEEI;
        private System.Windows.Forms.Label lblGEED;
        private System.Windows.Forms.Label lblGEOM;
        private System.Windows.Forms.Label lblGEEI;
        private System.Windows.Forms.Label lblGEGI;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Button btnGEU;
        private System.Windows.Forms.Button btnGEC;
        private System.Windows.Forms.Label lblGEGIS;
        private System.Windows.Forms.TextBox txtGEGI;
        private System.Windows.Forms.Button btnGES;
        private System.Windows.Forms.DataGridView dataGridViewGE;
        private System.Windows.Forms.DateTimePicker dateTimePickerA;
        private System.Windows.Forms.DataGridView dataGridViewAdvsior;
        private System.Windows.Forms.DataGridViewButtonColumn EDITAdvsior;
        private System.Windows.Forms.DataGridViewButtonColumn EditProjects;
        private System.Windows.Forms.DateTimePicker dateTimePickerPA;
        private System.Windows.Forms.Label lblPAPIS;
        private System.Windows.Forms.Button btnPAS;
        private System.Windows.Forms.DateTimePicker dateTimePickerGA;
        private System.Windows.Forms.DateTimePicker dateTimePickerGC;
        private System.Windows.Forms.DateTimePicker dateTimePickerED;
        private System.Windows.Forms.Button btnGCU;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSerchStudentGroups;
        private System.Windows.Forms.Button btnCGS;
        private System.Windows.Forms.DataGridViewButtonColumn EditEvaultions;
        private System.Windows.Forms.DataGridViewButtonColumn EditGroupEvalutions;
        private System.Windows.Forms.ErrorProvider errorProviderSFN;
        private System.Windows.Forms.ErrorProvider errorProviderSE;
        private System.Windows.Forms.DataGridViewButtonColumn EDIT;
        private System.Windows.Forms.ErrorProvider errorProviderSR;
        private System.Windows.Forms.ErrorProvider errorProviderdate;
        private System.Windows.Forms.ComboBox comboIA;
        private System.Windows.Forms.Label IndustrialAdvsior;
        private System.Windows.Forms.ComboBox comboCoAd;
        private System.Windows.Forms.Label CoAdvsior;
        private System.Windows.Forms.DataGridViewButtonColumn DeactivateStudent;
        private System.Windows.Forms.NumericUpDown txtGEOM;
    }
}

