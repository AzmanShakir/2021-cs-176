﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace FYP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ////Student Grid View Data Load
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT P.Id, FirstName,LastName,Contact,Email,DateOfBirth,Gender,RegistrationNo FROM Person AS P JOIN Student AS S ON P.Id = S.Id WHERE P.Id not in(select Id FROM Advisor)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            StudentGV.DataSource = dt;
            cmd.ExecuteNonQuery();
            tablelayout.SelectedIndex = 1;

        }

        private void StudentGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        private void btnRead_Click(object sender, EventArgs e)
        {

        }

        private void btnCDelete_Click(object sender, EventArgs e)
        {

        }

        private void btnCCreate_Click(object sender, EventArgs e)
        {

        }

        private void btnCUpdate_Click(object sender, EventArgs e)
        {

        }

        private void btnCRead_Click(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

        }

        private void btnUnRegister_Click(object sender, EventArgs e)
        {

        }

        private void btnView_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }



        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void btnSU_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT P.Id, FirstName,LastName,Contact,Email,DateOfBirth,Gender,RegistrationNo FROM Person AS P JOIN Student AS S ON P.Id = S.Id WHERE P.Id not in(select Id FROM Advisor) ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            StudentGV.DataSource = dt;
            ///clear functions
            txtSF.Clear();
            txtSL.Clear();
            txTSC.Clear();
            txtSE.Clear();
            txtSR.Clear();
            DATESD.CustomFormat = " ";
            DATESD.Format = DateTimePickerFormat.Custom;
            COMSG.Text = " ";

        }

        private void btnSC_Click(object sender, EventArgs e)
        {
            bool flag = false;
          
            if (string.IsNullOrEmpty(txtSE.Text))
            {
                errorProviderSFN.SetError(txtSE, "Please Enter Your EMAIL");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(txtSE,null);


            }
            if (string.IsNullOrEmpty(txtSF.Text))
            {
                errorProviderSE.SetError(txtSF, "Please Enter Your First Name");
                flag = true;

            }
            else
            {
                errorProviderSE.SetError(txtSF, null);

            }
            if (string.IsNullOrEmpty(txtSR.Text))
            {
                errorProviderSR.SetError(txtSR, "Please Enter Your Registrtion Number");
                flag = true;

            }
            else
            {
                errorProviderSR.SetError(txtSR, null);

            }
            if (flag)
            {
                return;
            }
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName, @LastName,@Contact,@Email,@DateOfBirth,@Gender)", con);
            cmd.Parameters.AddWithValue("@FirstName", txtSF.Text);
            cmd.Parameters.AddWithValue("@LastName", txtSL.Text);
            cmd.Parameters.AddWithValue("@Contact", txTSC.Text);
            cmd.Parameters.AddWithValue("@Email", txtSE.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", DATESD.Value);
            if (COMSG.Text == "Male")
            {
                cmd.Parameters.AddWithValue("@Gender", 1);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Gender", 2);

            }
            cmd.ExecuteNonQuery();
            int Id = 0;
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "SELECT Id FROM Person";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Id = reader.GetInt32(0);
                            }
                            reader.Close();
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            var studenttableinsert = Configuration.getInstance().getConnection();
            SqlCommand studenttableinsert1 = new SqlCommand("Insert into Student values (@Id, @RegistrationNo)", studenttableinsert);
            studenttableinsert1.Parameters.AddWithValue("@Id", Id);
            studenttableinsert1.Parameters.AddWithValue("@RegistrationNo", txtSR.Text);
            studenttableinsert1.ExecuteNonQuery();
            btnSU.PerformClick();

        }

        private void StudentGV_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                bool flag = false;

                if (string.IsNullOrEmpty(txtSE.Text))
                {
                    errorProviderSFN.SetError(txtSE, "Please Enter Your EMAIL");
                    flag = true;
                }
                else
                {
                    errorProviderSFN.SetError(txtSE, null);


                }
                if (string.IsNullOrEmpty(txtSF.Text))
                {
                    errorProviderSE.SetError(txtSF, "Please Enter Your First Name");
                    flag = true;

                }
                else
                { 
                    errorProviderSE.SetError(txtSF, null);

                }
                if (string.IsNullOrEmpty(txtSR.Text))
                {
                    errorProviderSR.SetError(txtSR, "Please Enter Your Registrtion Number");
                    flag = true;

                }
                else
                {
                    errorProviderSR.SetError(txtSR, null);

                }
                if (flag)
                {
                    return;
                }

                DataGridViewRow row = this.StudentGV.Rows[e.RowIndex];
                string id = row.Cells["Id"].Value.ToString();
                var con = Configuration.getInstance().getConnection();

                if (txtSF.Text != "" && txtSF.Text != row.Cells["FirstName"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName  = @FirstName  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@FirstName", txtSF.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtSL.Text != "" && txtSL.Text != row.Cells["LastName"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET LastName  = @LastName  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@LastName", txtSL.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txTSC.Text != "" && txTSC.Text != row.Cells["Contact"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Contact = @Contact  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Contact", txTSC.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtSE.Text != "" && txtSE.Text != row.Cells["Email"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Email  = @Email WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Email", txtSE.Text);
                    cmd.ExecuteNonQuery();
                }
                if (DATESD.Value.ToString() != row.Cells["DateOfBirth"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET DateOfBirth = @DateOfBirth  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@DateOfBirth", DATESD.Value);
                    cmd.ExecuteNonQuery();
                }
                if (COMSG.Text != null && DATESD.Text != row.Cells["Gender"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Gender = @Gender  WHERE Id = " + id, con);
                    if (COMSG.Text == "Male")
                    {
                        cmd.Parameters.AddWithValue("@Gender", 1);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Gender", 2);

                    }
                    cmd.ExecuteNonQuery();
                }
                if (txtSR.Text != "" && txtSR.Text != row.Cells["RegistrationNo"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Student SET RegistrationNo  = @RegistrationNo WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@RegistrationNo", txtSR.Text);
                    cmd.ExecuteNonQuery();
                }

            }
            btnSU.PerformClick();

        }

        private void btnSs_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Person.Id, FirstName,LastName,Contact,Email,DateOfBirth,Gender,RegistrationNo FROM Person JOIN Student ON Person.Id = Student.Id WHERE RegistrationNo = @RegistrationNo", con);
            cmd.Parameters.AddWithValue("@RegistrationNo", txtSRS.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            StudentGV.DataSource = dt;
            txtSRS.Clear();
        }

        private void btnAU_Click(object sender, EventArgs e)
        {
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT P.Id, FirstName,LastName,Contact,Email,DateOfBirth,Gender,Designation,Salary FROM Person AS P JOIN Advisor AS A ON P.Id = A.Id WHERE P.Id not in(select Id FROM Student)", con1);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewAdvsior.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            ///clear functions
            txtAF.Clear();
            txtAL.Clear();
            txtAC.Clear();
            txtAE.Clear();
            txtSR.Clear();
            dateTimePickerA.CustomFormat = " ";
            dateTimePickerA.Format = DateTimePickerFormat.Custom;
            COAD.Text = " ";
            txtAS.Clear();
            coAG.Text = " ";
        }

        private void btnAC_Click(object sender, EventArgs e)
        {
            bool flag = false;

            if (string.IsNullOrEmpty(txtAF.Text))
            {
                errorProviderSFN.SetError(txtAF, "Please Enter Your Name");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(txtAF, null);


            }
            if (string.IsNullOrEmpty(txtAE.Text))
            {
                errorProviderSE.SetError(txtAE, "Please Enter Your  Email");
                flag = true;

            }
            else
            {
                errorProviderSE.SetError(txtAE, null);

            }
            if (COAD.SelectedItem == null)
            {
                errorProviderSR.SetError(COAD, "Please Enter Your Designation");
                flag = true;

            }
            else
            {
                errorProviderSR.SetError(COAD, null);

            }
            if (string.IsNullOrEmpty(txtAS.Text))
            {
                errorProviderdate.SetError(txtAS, "Please Enter Your  Salary");
                flag = true;

            }
            else
            {
                errorProviderdate.SetError(txtAS, null);

            }

            if (flag)
            {
                return;
            }
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName, @LastName,@Contact,@Email,@DateOfBirth,@Gender)", con);
            cmd.Parameters.AddWithValue("@FirstName", txtAF.Text);
            cmd.Parameters.AddWithValue("@LastName", txtAL.Text);
            cmd.Parameters.AddWithValue("@Contact", txtAC.Text);
            cmd.Parameters.AddWithValue("@Email", txtAE.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", dateTimePickerA.Value);
            if (COMSG.Text == "Male")
            {
                cmd.Parameters.AddWithValue("@Gender", 1);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Gender", 2);

            }
            cmd.ExecuteNonQuery();
            int Id = 0;
            string connectionString = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "SELECT Id FROM Person";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Id = reader.GetInt32(0);
                            }
                            reader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            SqlCommand advsiortableinsert1 = new SqlCommand("Insert into Advisor values (@Id, @Designation,@Salary)", con);
            advsiortableinsert1.Parameters.AddWithValue("@Id", Id);
            ///for lookup designation
            SqlCommand cmd1 = new SqlCommand("SELECT Id from Lookup where Value = @Value", con);
            cmd1.Parameters.AddWithValue("@Value", COAD.Text);
            advsiortableinsert1.Parameters.AddWithValue("@Designation", Convert.ToInt32(cmd1.ExecuteScalar()));
            ////////////////////////////
            advsiortableinsert1.Parameters.AddWithValue("@Salary", int.Parse(txtAS.Text));
            advsiortableinsert1.ExecuteNonQuery();

            btnAU.PerformClick();
        }

        private void dataGridViewAdvsior_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                bool flag = false;

                if (string.IsNullOrEmpty(txtAF.Text))
                {
                    errorProviderSFN.SetError(txtAF, "Please Enter Your Name");
                    flag = true;
                }
                else
                {
                    errorProviderSFN.SetError(txtAF, null);


                }
                if (string.IsNullOrEmpty(txtAE.Text))
                {
                    errorProviderSE.SetError(txtAE, "Please Enter Your  Email");
                    flag = true;

                }
                else
                {
                    errorProviderSE.SetError(txtAE, null);

                }
                if (COAD.SelectedItem == null)
                {
                    errorProviderSR.SetError(COAD, "Please Enter Your Designation");
                    flag = true;

                }
                else
                {
                    errorProviderSR.SetError(COAD, null);

                }
                if (string.IsNullOrEmpty(txtAS.Text))
                {
                    errorProviderdate.SetError(txtAS, "Please Enter Your  Salary");
                    flag = true;

                }
                else
                {
                    errorProviderdate.SetError(txtAS, null);

                }

                if (flag)
                {
                    return;
                }
                DataGridViewRow row = this.dataGridViewAdvsior.Rows[e.RowIndex];
                string id = row.Cells["Id"].Value.ToString();
                var con = Configuration.getInstance().getConnection();

                if (txtAF.Text != "" && txtAF.Text != row.Cells["FirstName"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName  = @FirstName  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@FirstName", txtAF.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtAL.Text != "" && txtAL.Text != row.Cells["LastName"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET LastName  = @LastName  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@LastName", txtAL.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtAC.Text != "" && txtAC.Text != row.Cells["Contact"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Contact = @Contact  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Contact", txtAC.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtAE.Text != "" && txtAE.Text != row.Cells["Email"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Email  = @Email WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Email", txtAE.Text);
                    cmd.ExecuteNonQuery();
                }
                if (dateTimePickerA.Value.ToString() != row.Cells["DateOfBirth"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET DateOfBirth = @DateOfBirth  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@DateOfBirth", dateTimePickerA.Value);
                    cmd.ExecuteNonQuery();
                }
                if (coAG.Text != null && coAG.Text != row.Cells["Gender"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Person SET Gender = @Gender  WHERE Id = " + id, con);
                    if (coAG.Text == "Male")
                    {
                        cmd.Parameters.AddWithValue("@Gender", 1);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Gender", 2);

                    }
                    cmd.ExecuteNonQuery();
                }
                if (COAD.Text != null && COAD.Text != row.Cells["Designation"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Advisor SET Designation = @Designation  WHERE Id = " + id, con);
                    ///for lookup designation
                    SqlCommand cmd1 = new SqlCommand("SELECT Id from Lookup where Value = @Value", con);
                    cmd1.Parameters.AddWithValue("@Value", COAD.Text);
                    var temp = cmd1.ExecuteScalar();
                    cmd.Parameters.AddWithValue("@Designation", Convert.ToInt32(temp));
                    cmd.ExecuteNonQuery();
                }
                if (txtAS.Text != "" && txtAS.Text != row.Cells["Salary"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Advisor SET Salary  = @Salary WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Salary", int.Parse(txtAS.Text));
                    cmd.ExecuteNonQuery();
                }

            }
            btnAU.PerformClick();

        }

        private void COAD_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAS_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT FirstName,LastName,Contact,Email,DateOfBirth,Gender,Designation,Salary FROM Person JOIN Advisor ON Person.Id = Advisor.Id Where FirstName  = @FirstName", con);
            cmd.Parameters.AddWithValue("@FirstName", txtANS.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridViewAdvsior.DataSource = dt;
            txtANS.Clear();
        }

        private void txtANS_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnPU_Click(object sender, EventArgs e)
        {
            var con2 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM Project AS P", con2);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewP.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            txtPT.Clear();
            txtPD.Clear();
        }

        private void btnPC_Click(object sender, EventArgs e)
        {

            bool flag = false;

            if (string.IsNullOrEmpty(txtPT.Text))
            {
                errorProviderSFN.SetError(txtPT, "Please Enter Tilte of Project");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(txtPT, null);


            }
            if (flag)
            {
                return;
            }
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Project values (@Title, @Description)", con);
            cmd.Parameters.AddWithValue("@Title", txtPD.Text);
            cmd.Parameters.AddWithValue("@Description", txtPT.Text);
            cmd.ExecuteNonQuery();
            btnPU.PerformClick();
        }

        private void dataGridViewP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                bool flag = false;

                if (string.IsNullOrEmpty(txtPT.Text))
                {
                    errorProviderSFN.SetError(txtPT, "Please Enter Tilte of Project");
                    flag = true;
                }
                else
                {
                    errorProviderSFN.SetError(txtPT, null);


                }
                if (flag)
                {
                    return;
                }
                DataGridViewRow row = this.dataGridViewP.Rows[e.RowIndex];
                string id = row.Cells["Id"].Value.ToString();
                var con = Configuration.getInstance().getConnection();

                if (txtPT.Text != "" && txtPT.Text != row.Cells["Title"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Project SET Title  = @Title  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Title", txtPD.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtPD.Text != "" && txtPD.Text != row.Cells["Description"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Project SET Description  = @Description  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Description", txtPT.Text);
                    cmd.ExecuteNonQuery();
                }

                btnPU.PerformClick();

            }

        }

        private void btnS_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project AS P  Where Title  = @Title", con);
            cmd.Parameters.AddWithValue("@Title", txtPTS.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridViewP.DataSource = dt;
            txtPTS.Clear();
        }

        private void tablelayout_Click(object sender, EventArgs e)
        {

        }

        private void comPAAI_Click(object sender, EventArgs e)
        {
  



            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "Select Id from Advisor where ID NOT IN (SELECT AdvisorId from ProjectAdvisor where ProjectId = @var)";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                    da.SelectCommand.Parameters.Add("@var", int.Parse(comPAPI.Text));
                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Advisor");
                    comPAAI.DisplayMember = "Id";
                    comPAAI.DataSource = ds.Tables["Advisor"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show("You have to choose Project ID first before chossing this");
                }
            }


        }

        private void comPAPI_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project Where Id not in(Select [ProjectId] from [ProjectAdvisor])";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Project");
                    comPAPI.DisplayMember = "Id";
                    comPAPI.DataSource = ds.Tables["Project"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void btnPAC_Click(object sender, EventArgs e)
        {
            bool flag = false;

            if (comPAAI.SelectedItem == null)
            {
                errorProviderSFN.SetError(comPAAI, "Please Enter Main Advsior");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(comPAAI, null);


            }
            if (comPAPI.SelectedItem == null)
            {
                errorProviderSE.SetError(comPAPI, "Please Enter Your Project I'd");
                flag = true;

            }
            else
            {
                errorProviderSE.SetError(comPAPI, null);

            }
            if (comboCoAd.SelectedItem == null)
            {
                errorProviderSR.SetError(comboCoAd, "Please Enter Your Co Advsior Role");
                flag = true;

            }
            else
            {
                errorProviderSR.SetError(comboCoAd, null);

            }
            if (comboIA.SelectedItem == null)
            {
                errorProviderdate.SetError(comboIA, "Please Enter Your Indrustial Advsior Role");
                flag = true;

            }
            else
            {
                errorProviderdate.SetError(comboIA, null);

            }
            if (flag)
            {
                return;
            }
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId, @ProjectId,@AdvisorRole,@AssignmentDate)", con);
            cmd.Parameters.AddWithValue("@AdvisorId", int.Parse(comPAAI.Text));
            cmd.Parameters.AddWithValue("@ProjectId", int.Parse(comPAPI.Text));
            cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePickerPA.Value);
            cmd.Parameters.AddWithValue("@AdvisorRole",11);
            cmd.ExecuteNonQuery();
            var con1 = Configuration.getInstance().getConnection();

            SqlCommand cmd1 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId1, @ProjectId1,@AdvisorRole1,@AssignmentDate1)", con1);
            cmd1.Parameters.AddWithValue("@AdvisorId1", int.Parse(comboCoAd.Text));
            cmd1.Parameters.AddWithValue("@ProjectId1", int.Parse(comPAPI.Text));
            cmd1.Parameters.AddWithValue("@AssignmentDate1", dateTimePickerPA.Value);
            cmd1.Parameters.AddWithValue("@AdvisorRole1",12);
            cmd1.ExecuteNonQuery();
            var con2 = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId2, @ProjectId2,@AdvisorRole2,@AssignmentDate2)", con2);
            cmd2.Parameters.AddWithValue("@AdvisorId2", int.Parse(comboIA.Text));
            cmd2.Parameters.AddWithValue("@ProjectId2", int.Parse(comPAPI.Text));
            cmd2.Parameters.AddWithValue("@AssignmentDate2", dateTimePickerPA.Value);
            cmd2.Parameters.AddWithValue("@AdvisorRole2", 14);
            cmd2.ExecuteNonQuery();
            btnPAU.PerformClick();
        }

        private void btnPAU_Click(object sender, EventArgs e)
        {
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT p.AdvisorId,p.ProjectId,p.AdvisorRole,p.AssignmentDate  FROM ProjectAdvisor as p", con1);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewPA.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            ///clear functions
            dateTimePickerPA.CustomFormat = " ";
            dateTimePickerPA.Format = DateTimePickerFormat.Custom;
            comPAPI.Text = " ";
            comPAAI.Text = " ";
            comboIA.Text = " ";
            comboCoAd.Text = " ";


        }

        private void dataGridViewPA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                bool flag = false;

                if (comPAAI.SelectedItem == null)
                {
                    errorProviderSFN.SetError(comPAAI, "Please Enter Main Advsior");
                    flag = true;
                }
                else
                {
                    errorProviderSFN.SetError(comPAAI, null);


                }
                if (comPAPI.SelectedItem == null)
                {
                    errorProviderSE.SetError(comPAPI, "Please Enter Your Project I'd");
                    flag = true;

                }
                else
                {
                    errorProviderSE.SetError(comPAPI, null);

                }
                if (comboCoAd.SelectedItem == null)
                {
                    errorProviderSR.SetError(comboCoAd, "Please Enter Your Co Advsior Role");
                    flag = true;

                }
                else
                {
                    errorProviderSR.SetError(comboCoAd, null);

                }
                if (comboIA.SelectedItem == null)
                {
                    errorProviderdate.SetError(comboIA, "Please Enter Your Indrustial Advsior Role");
                    flag = true;

                }
                else
                {
                    errorProviderdate.SetError(comboIA, null);

                }
                if (flag)
                {
                    return;
                }
                DataGridViewRow row = this.dataGridViewPA.Rows[e.RowIndex];
                string id = row.Cells["ProjectId"].Value.ToString();
                var con = Configuration.getInstance().getConnection();

                if (comPAAI.Text != row.Cells["AdvisorId"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorId  = @AdvisorId  WHERE [ProjectId] = " + id, con);
                    cmd.Parameters.AddWithValue("@AdvisorId", int.Parse(comPAAI.Text));
                    cmd.ExecuteNonQuery();
                }
                if (comPAPI.Text != row.Cells["ProjectId"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE  ProjectAdvisor SET ProjectId  = @ProjectId  WHERE [ProjectId] = " + id, con);
                    cmd.Parameters.AddWithValue("@ProjectId", int.Parse(comPAPI.Text));
                    cmd.ExecuteNonQuery();
                }
                if (dateTimePickerPA.Value.ToString() != row.Cells["AssignmentDate"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE ProjectAdvisor SET AssignmentDate = @AssignmentDate  WHERE [ProjectId] = " + id, con);
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePickerPA.Value);
                    cmd.ExecuteNonQuery();
                }

            }
            btnPAU.PerformClick();
        }

        private void btnPAS_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT p.AdvisorId,p.ProjectId,p.AdvisorRole,p.AssignmentDate  FROM ProjectAdvisor as p Where ProjectId  = @ProjectId", con);
            cmd.Parameters.AddWithValue("@ProjectId", int.Parse(txtPAPI.Text));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridViewPA.DataSource = dt;
            txtPAPI.Clear();
        }

        private void btnGC_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into [ProjectA].[dbo].[Group] values (@Created_On)", con);
            cmd.Parameters.AddWithValue("@Created_On", dateTimePickerGC.Value);
            cmd.ExecuteNonQuery();
            btnGCU.PerformClick();
        }

        private void btnGCU_Click(object sender, EventArgs e)
        {

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT p.Id,p.Created_On  FROM [ProjectA].[dbo].[Group] as p", con1);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewGC.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            ///clear functions
            dateTimePickerGC.Format = DateTimePickerFormat.Custom;
            dateTimePickerGC.CustomFormat = " ";



        }

        private void btnGPC_Click(object sender, EventArgs e)
        {
            bool flag = false;


            if (comGPGI.SelectedItem == null)
            {
                errorProviderSFN.SetError(comGPGI, "Please Enter Your Group Id");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(comGPGI, null);


            }
            if (comGPPI.SelectedItem == null)
            {
                errorProviderSE.SetError(comGPPI, "Please Enter Your Student I'd");
                flag = true;

            }
            else
            {
                errorProviderSE.SetError(comGPPI, null);

            }
            if (flag)
            {
                return;
            }
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into GroupStudent values (@GroupId, @StudentId,@Status,@AssignmentDate)", con);
            cmd.Parameters.AddWithValue("@GroupId", int.Parse(comGPGI.Text));
            cmd.Parameters.AddWithValue("@StudentId", int.Parse(comGPPI.Text));
            cmd.Parameters.AddWithValue("@Status", 3);
            cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePickerGA.Value);
            cmd.ExecuteNonQuery();
            btnGPU.PerformClick();
        }

        private void comPAAI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comPAPI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comGPGI_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "  Select Id from [ProjectA].[dbo].[Group] Where Id not in (Select GroupID From [ProjectA].[dbo].[GroupStudent] Where status = 3 group by GroupId having count(GroupId) >= 4)";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Group");
                    comGPGI.DisplayMember = "Id";
                    comGPGI.ValueMember = "Created_On";
                    comGPGI.DataSource = ds.Tables["Group"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void comGPPI_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "Select Id from [ProjectA].[dbo].[Student] where Id not in ( Select StudentId from [ProjectA].[dbo].[GroupStudent] where Status = 3)";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    comGPPI.DisplayMember = "Id";
                    comGPPI.DataSource = ds.Tables["Student"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void btnGPU_Click(object sender, EventArgs e)
        {
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT *  FROM [ProjectA].[dbo].[GroupStudent]", con1);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewGP.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            ///clear functions
            dateTimePickerGA.CustomFormat = " ";
            dateTimePickerGA.Format = DateTimePickerFormat.Custom;
            comGPGI.Text = " ";
            comGPPI.Text = " ";

        }

        private void btnCGS_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT *  FROM [ProjectA].[dbo].[GroupStudent] Where StudentId  = @StudentId", con);
            cmd.Parameters.AddWithValue("@StudentId", int.Parse(txtSerchStudentGroups.Text));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridViewGP.DataSource = dt;
            txtSerchStudentGroups.Clear();
        }

        private void btnEA_Click(object sender, EventArgs e)
        {
            bool flag = false;

            if (string.IsNullOrEmpty(txtEN.Text))
            {
                errorProviderSFN.SetError(txtEN, "Please Enter Your Name");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(txtEN, null);


            }
            if (string.IsNullOrEmpty(txtTM.Text))
            {
                errorProviderSE.SetError(txtTM, "Please Enter Your Total Marks");
                flag = true;

            }
            else
            {
                errorProviderSE.SetError(txtTM, null);

            }
            if (string.IsNullOrEmpty(txtTW.Text))
            {
                errorProviderSR.SetError(txtTW, "Please Enter Your Total Wiegthage");
                flag = true;

            }
            else
            {
                errorProviderSR.SetError(txtTW, null);

            }
            if (flag)
            {
                return;
            }

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Evaluation values (@Name, @TotalMarks,@TotalWeightage)", con);
            cmd.Parameters.AddWithValue("@Name", txtEN.Text);
            cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(txtTM.Text));
            cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(txtTW.Text));
            cmd.ExecuteNonQuery();
            btnEU.PerformClick();
        }

        private void btnEU_Click(object sender, EventArgs e)
        {
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT *  FROM [ProjectA].[dbo].[Evaluation]", con1);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewE.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            txtEN.Clear();
            txtTM.Clear();
            txtTW.Clear();
        }

        private void dataGridViewE_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                bool flag = false;

                if (string.IsNullOrEmpty(txtEN.Text))
                {
                    errorProviderSFN.SetError(txtEN, "Please Enter Your Name");
                    flag = true;
                }
                else
                {
                    errorProviderSFN.SetError(txtEN, null);


                }
                if (string.IsNullOrEmpty(txtTM.Text))
                {
                    errorProviderSE.SetError(txtTM, "Please Enter Your Total Marks");
                    flag = true;

                }
                else
                {
                    errorProviderSE.SetError(txtTM, null);

                }
                if (string.IsNullOrEmpty(txtTW.Text))
                {
                    errorProviderSR.SetError(txtTW, "Please Enter Your Total Wiegthage");
                    flag = true;

                }
                else
                {
                    errorProviderSR.SetError(txtTW, null);

                }
                if (flag)
                {
                    return;
                }
                DataGridViewRow row = this.dataGridViewE.Rows[e.RowIndex];
                string id = row.Cells["Id"].Value.ToString();
                var con = Configuration.getInstance().getConnection();

                if (txtEN.Text != "" && txtEN.Text != row.Cells["Name"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Evaluation SET Name  = @Name  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@Name", txtEN.Text);
                    cmd.ExecuteNonQuery();
                }
                if (txtTM.Text != "" && txtTM.Text != row.Cells["TotalMarks"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE  Evaluation SET TotalMarks  = @TotalMarks  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(txtTM.Text));
                    cmd.ExecuteNonQuery();
                }
                if (txtTW.Text != "" && txtTW.Text != row.Cells["TotalWeightage"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE Evaluation SET TotalWeightage = @TotalWeightage  WHERE Id = " + id, con);
                    cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(txtTW.Text));
                    cmd.ExecuteNonQuery();
                }

            }
            btnEU.PerformClick();
        }

        private void txtSerchStudentGroups_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnES_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT *  FROM [ProjectA].[dbo].[Evaluation] Where Name  = @Name", con);
            cmd.Parameters.AddWithValue("@Name", txtENS.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridViewE.DataSource = dt;
            txtENS.Clear();
        }

        private void dataGridViewGP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                DataGridViewRow row = this.dataGridViewGP.Rows[e.RowIndex];
                string id = row.Cells["StudentId"].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE GroupStudent SET Status  = 4  WHERE StudentId = " + id, con);
                cmd.ExecuteNonQuery();
            }
            btnGPU.PerformClick();
        }

        private void comGPGI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comGEGI_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from [ProjectA].[dbo].[Group]";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Group");
                    comGEGI.DisplayMember = "Id";
                    comGEGI.DataSource = ds.Tables["Group"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void comGEEI_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "Select Id from [ProjectA].[dbo].[Evaluation] where id not in(select EvaluationId from [ProjectA].[dbo].[GroupEvaluation] where GroupId = @GroupId)";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                    da.SelectCommand.Parameters.Add("@GroupId", int.Parse(comGEGI.Text));
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Evaluation");
                    comGEEI.DisplayMember = "Id";
                    comGEEI.DataSource = ds.Tables["Evaluation"];

                }
                catch (Exception exception)
                {

                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void btnGEU_Click(object sender, EventArgs e)
        {
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT *  FROM [ProjectA].[dbo].[GroupEvaluation]", con1);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridViewGE.DataSource = dt1;
            cmd1.ExecuteNonQuery();
            comGEGI.Text = " ";
            comGEEI.Text = " ";
            txtPAPI.Clear();
            ///clear functions
            dateTimePickerED.CustomFormat = " ";
            dateTimePickerED.Format = DateTimePickerFormat.Custom;
        }

        private void btnGEC_Click(object sender, EventArgs e)
        {
            bool flag = false;


            if (comGEGI.SelectedItem == null)
            {
                errorProviderSFN.SetError(comGEGI, "Please Enter Your Group Id");
                flag = true;
            }
            else
            {
                errorProviderSFN.SetError(comGEGI, null);


            }
            if (comGEEI.SelectedItem == null)
            {
                errorProviderSE.SetError(comGEEI, "Please Enter Your Evaltuion I'd");
                flag = true;

            }
            else
            {
                errorProviderSE.SetError(comGEEI, null);

            }
            if (string.IsNullOrEmpty(txtGEOM.Text))
            {
                errorProviderSR.SetError(txtGEOM, "Please Enter Your Total Obtained Marks");
                flag = true;

            }
            else
            {
                errorProviderSR.SetError(txtGEOM, null);

            }
            if (flag)
            {
                return;
            }
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into GroupEvaluation values (@GroupId, @EvaluationId,@ObtainedMarks,@EvaluationDate)", con);
            cmd.Parameters.AddWithValue("@GroupId", int.Parse(comGEGI.Text));
            cmd.Parameters.AddWithValue("@EvaluationId", int.Parse(comGEEI.Text));
            cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePickerED.Value);
            cmd.Parameters.AddWithValue("@ObtainedMarks", txtGEOM.Text);
            cmd.ExecuteNonQuery();
            btnGEU.PerformClick();

        }

        private void btnGES_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT *  FROM [ProjectA].[dbo].[GroupEvaluation] Where GroupId  = @GroupId", con);
            cmd.Parameters.AddWithValue("@GroupId", txtGEGI.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridViewGE.DataSource = dt;
            txtGEGI.Clear();
        }

        private void dataGridViewGE_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                bool flag = false;


                if (comGEGI.SelectedItem == null)
                {
                    errorProviderSFN.SetError(comGEGI, "Please Enter Your Group Id");
                    flag = true;
                }
                else
                {
                    errorProviderSFN.SetError(comGEGI, null);


                }
                if (comGEEI.SelectedItem == null)
                {
                    errorProviderSE.SetError(comGEEI, "Please Enter Your Evaltuion I'd");
                    flag = true;

                }
                else
                {
                    errorProviderSE.SetError(comGEEI, null);

                }
                if (string.IsNullOrEmpty(txtGEOM.Text))
                {
                    errorProviderSR.SetError(txtGEOM, "Please Enter Your Total Obtained Marks");
                    flag = true;

                }
                else
                {
                    errorProviderSR.SetError(txtGEOM, null);

                }
                if (flag)
                {
                    return;
                }
                DataGridViewRow row = this.dataGridViewGE.Rows[e.RowIndex];
                string id = row.Cells["GroupId"].Value.ToString();
                var con = Configuration.getInstance().getConnection();

                if (comGEGI.Text != "" && comGEGI.Text != row.Cells["GroupId"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE GroupEvaluation SET GroupId  = @GroupId  WHERE GroupId = " + id, con);
                    cmd.Parameters.AddWithValue("@GroupId", int.Parse(comGEGI.Text));
                    cmd.ExecuteNonQuery();
                }
                if (comGEEI.Text != "" && comGEEI.Text != row.Cells["EvaluationId"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE GroupEvaluation SET EvaluationId  = @EvaluationId  WHERE GroupId = " + id, con);
                    cmd.Parameters.AddWithValue("@EvaluationId", int.Parse(comGEEI.Text));
                    cmd.ExecuteNonQuery();
                }
                if (txtGEOM.Text != "" && txtGEOM.Text != row.Cells["ObtainedMarks"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE GroupEvaluation SET ObtainedMarks = @ObtainedMarks  WHERE GroupId = " + id, con);
                    cmd.Parameters.AddWithValue("@ObtainedMarks", int.Parse(txtGEOM.Text));
                    cmd.ExecuteNonQuery();
                }
                if (dateTimePickerED.Value.ToString() != row.Cells["EvaluationDate"].Value.ToString())
                {
                    SqlCommand cmd = new SqlCommand("UPDATE GroupEvaluation SET EvaluationDate = @EvaluationDate  WHERE GroupId = " + id, con);
                    cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePickerED.Value);
                    cmd.ExecuteNonQuery();
                }


            }
            btnGEU.PerformClick();
        }

        private void tableLayoutPanel13_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtSF_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void txtSE_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void txtSF_Leave(object sender, EventArgs e)
        {

        }

        private void tablelayout_ContextMenuStripChanged(object sender, EventArgs e)
        {
        }

        private void TPStudent_Enter(object sender, EventArgs e)
        {

        }

        private void MainAdvsior_Click(object sender, EventArgs e)
        {

        }

        private void comboCoAd_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "Select Id from Advisor where Id NOT IN (SELECT AdvisorId from ProjectAdvisor where ProjectId = @var) AND Id <> @var1";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                    da.SelectCommand.Parameters.Add("@var", int.Parse(comPAPI.Text));
                    da.SelectCommand.Parameters.Add("@var1", int.Parse(comPAAI.Text));
                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Advisor");
                    comboCoAd.DisplayMember = "Id";
                    comboCoAd.DataSource = ds.Tables["Advisor"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show("You have to  choose main Adversior ID first before chosing this");
                }
            }
        }

        private void comboIA_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectA;Integrated Security=True"))
            {
                try
                {
                    string query = "Select Id from Advisor where Id NOT IN (SELECT AdvisorId from ProjectAdvisor where ProjectId = @var) AND Id <> @var1 AND Id <> @var2";
                    SqlDataAdapter da = new SqlDataAdapter(query, con);
                    da.SelectCommand.Parameters.Add("@var", int.Parse(comPAPI.Text));
                    da.SelectCommand.Parameters.Add("@var1", int.Parse(comboCoAd.Text));
                    da.SelectCommand.Parameters.Add("@var2", int.Parse(comPAAI.Text));

                     
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Advisor");
                    comboIA.DisplayMember = "Id";
                    comboIA.DataSource = ds.Tables["Advisor"];
                }
                catch (Exception exception)
                {

                    MessageBox.Show("You have to choose Co=Advsior ID first before chossing this");
                }
            }
        }

        private void txtSF_TextChanged(object sender, EventArgs e)
        {

        }

        private void comGEEI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtGEOM_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtGEOM_Click(object sender, EventArgs e)
        {
            int Id = 0;
            string connectionString1 = @"Data Source=(local);Initial Catalog=ProjectA;Integrated Security=True";
            string query = "Select TotalMarks FROM [ProjectA].[dbo].[Evaluation] Where Id = @Id";

            using (SqlConnection connection1 = new SqlConnection(connectionString1))
            {
                using (SqlCommand command1 = new SqlCommand(query, connection1))
                {
                    try
                    {
                        command1.Parameters.AddWithValue("@Id", int.Parse(comGEEI.Text));
                        using (SqlDataReader reader = command1.ExecuteReader())
                        {
                            lblGEED.Text = "Azman";
                            while (reader.Read())
                            {
                                Id = reader.GetInt32(0);
                                txtGEOM.Maximum = Id;


                            }
                            reader.Close();
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }
    }
}
